<?php

namespace ServiceCore\PaymentGateway\Authorize\Adapter\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\PaymentGateway\Authorize\Adapter\Card as CardAdapter;
use ServiceCore\PaymentGateway\Authorize\Gateway\CIMGateway;

class Card implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CardAdapter
    {
        $gateway = new CIMGateway();

        return new CardAdapter($gateway);
    }
}
