<?php

namespace ServiceCore\PaymentGateway\Stripe\Adapter\Factory\Delegator;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use ServiceCore\PaymentGateway\Stripe\Adapter\Bank as BankAdapter;

class BankDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        $context = $callback();

        if (\method_exists($context, 'setAdapter')) {
            /** @var BankAdapter $adapter */
            $adapter = $container->get(BankAdapter::class);

            $context->setAdapter($adapter);
        }

        return $context;
    }
}
