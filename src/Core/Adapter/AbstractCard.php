<?php

namespace ServiceCore\PaymentGateway\Core\Adapter;

use ServiceCore\PaymentGateway\Core\Data\BillingAddress;
use ServiceCore\PaymentGateway\Core\Data\CreditCard;
use ServiceCore\PaymentGateway\Core\Data\CreditCardUser;
use ServiceCore\PaymentGateway\Core\Data\Token;

abstract class AbstractCard
{
    abstract public function createCard(CreditCardUser $cardUser, Token $token, ?BillingAddress $billingAddress = null);

    abstract public function deleteCard(CreditCard $card);
    
    abstract public function retrieveCard(string $payerToken, string $cardToken);
}
