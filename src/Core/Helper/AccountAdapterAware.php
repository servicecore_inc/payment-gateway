<?php

namespace ServiceCore\PaymentGateway\Core\Helper;

use RuntimeException;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractAccount;

trait AccountAdapterAware
{
    /** @var AbstractAccount */
    private $accountAdapter;

    public function getAccountAdapter(): AbstractAccount
    {
        if (!$this->accountAdapter) {
            throw new RuntimeException('Account adapter is not set');
        }

        return $this->accountAdapter;
    }

    public function setAccountAdapter(AbstractAccount $adapter): self
    {
        $this->accountAdapter = $adapter;

        return $this;
    }
}
