<?php

namespace ServiceCore\PaymentGateway\Test\Support;

use ServiceCore\PaymentGateway\Core\Data\Payment;

class MockPayment implements Payment
{
    /** @var int|null */
    private $lastFour;

    /** @var string|null */
    private $expiry;

    /** @var null|string */
    private $transactionId;

    /** @var mixed */
    private $amount;

    public function __construct(
        ?string $lastFour = null,
        ?string $transactionId = null,
        ?int $amount = null,
        ?string $expiry = null
    ) {
        $this->lastFour      = $lastFour;
        $this->expiry        = $expiry;
        $this->transactionId = $transactionId;
        $this->amount        = $amount;
    }

    public function getLastFour(): ?string
    {
        return $this->lastFour;
    }

    public function getExpiry(): ?string
    {
        return $this->expiry;
    }

    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    public function getAmount()
    {
        return $this->amount;
    }
}
