<?php

namespace ServiceCore\PaymentGateway\CardConnect\Adapter\Delegator;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use ServiceCore\PaymentGateway\CardConnect\Adapter\Payment as PaymentAdapter;

class PaymentDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null): object
    {
        $context = $callback();

        if (\method_exists($context, 'setAdapter')) {
            $adapter = $container->get(PaymentAdapter::class);

            $context->setAdapter($adapter);
        }

        return $context;
    }
}
