<?php

namespace ServiceCore\PaymentGateway\CardConnect\Adapter;

use ServiceCore\PaymentGateway\CardConnect\Helper\ClientAware;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractCard;
use ServiceCore\PaymentGateway\Core\Data\BillingAddress;
use ServiceCore\PaymentGateway\Core\Data\CreditCard;
use ServiceCore\PaymentGateway\Core\Data\CreditCardUser;
use ServiceCore\PaymentGateway\Core\Data\Token;

class Card extends AbstractCard implements ClientAwareInterface
{
    use ClientAware;

    public function createCard(CreditCardUser $cardUser, Token $token, ?BillingAddress $billingAddress = null): array
    {
        if ($billingAddress) {
            $billing = [
                'address' => $billingAddress->getAddress(),
                'city'    => $billingAddress->getCity(),
                'region'  => $billingAddress->getState(),
                'postal'  => $billingAddress->getZip()
            ];
        } else {
            $billing = [
                'address' => $cardUser->getAddress(),
                'city'    => $cardUser->getCity(),
                'region'  => $cardUser->getStateCode(),
                'postal'  => $cardUser->getZip()
            ];
        }

        return $this->client->profileCreate(\array_merge(
            [
                'profile'     => $cardUser->getGatewayId(),
                'merchid'     => $this->client->getMerchantId(),
                'defaultacct' => 'Y',
                'account'     => $token->getValue(),
                'name'        => $token->getAuthority() ?: $cardUser->getName()
            ],
            $billing
        ));
    }

    public function deleteCard(CreditCard $card): array
    {
        return $this->client->profileDelete($card->getCreditCardUser()->getGatewayId(), $card->getToken());
    }

    public function retrieveCard(string $payerToken, string $cardToken): array
    {
        return $this->client->accountGet($payerToken, $cardToken);
    }

    public function updateCard(CreditCard $card): array
    {
        return $this->client->profileCreate(
            [
                'profile'       => $card->getCreditCardUser()->getGatewayId() . '/' . $card->getToken(),
                'defaultacct'   => 'Y',
                'profileupdate' => 'Y',
                'merchid'       => $this->client->getMerchantId()
            ]
        );
    }
}
