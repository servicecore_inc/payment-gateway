<?php

namespace ServiceCore\PaymentGateway\Core\Helper;

use ServiceCore\PaymentGateway\Core\Adapter\AbstractCard;

interface CardAdapterAwareInterface
{
    public function getCardAdapter(): AbstractCard;

    public function setCardAdapter(AbstractCard $bankAdapter);
}
