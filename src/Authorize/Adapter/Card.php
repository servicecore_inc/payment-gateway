<?php

namespace ServiceCore\PaymentGateway\Authorize\Adapter;

use Omnipay\Common\Message\ResponseInterface;
use ServiceCore\PaymentGateway\Authorize\Adapter\Payment as PaymentAdapter;
use ServiceCore\PaymentGateway\Authorize\Gateway\CIMGateway;
use ServiceCore\PaymentGateway\Authorize\Helper\DeveloperEndpointAware;
use ServiceCore\PaymentGateway\Authorize\Helper\GatewayCredentialAware;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractCard;
use ServiceCore\PaymentGateway\Core\Data\BillingAddress;
use ServiceCore\PaymentGateway\Core\Data\CreditCard;
use ServiceCore\PaymentGateway\Core\Data\CreditCardUser;
use ServiceCore\PaymentGateway\Core\Data\Token;

class Card extends AbstractCard
{
    use DeveloperEndpointAware;
    use GatewayCredentialAware;

    /** @var CIMGateway */
    private $CIMGateway;

    public function __construct(CIMGateway $CIMGateway)
    {
        $this->CIMGateway = $CIMGateway;
    }

    public function createCard(
        CreditCardUser $cardUser,
        Token $token,
        ?BillingAddress $billingAddress = null
    ): ResponseInterface {
        $firstName = $cardUser->getFirstName();
        $lastName  = $cardUser->getLastName();

        if ($tokenAuthority = $token->getAuthority()) {
            $exploded = \explode(' ', $tokenAuthority);
            if (\count($exploded) === 2) {
                [$firstName, $lastName] = $exploded;
            } else {
                $firstName = $tokenAuthority;
                $lastName  = null;
            }
        }

        if ($billingAddress) {
            $billingData = [
                'firstName' => $firstName,
                'lastName'  => $lastName,
                'address'   => $billingAddress->getAddress(),
                'city'      => $billingAddress->getCity(),
                'state'     => $billingAddress->getState(),
                'zip'       => $billingAddress->getZip()
            ];
        } else {
            $billingData = [
                'firstName' => $firstName,
                'lastName'  => $lastName,
                'address'   => $cardUser->getAddress(),
                'city'      => $cardUser->getCity(),
                'state'     => $cardUser->getStateCode(),
                'zip'       => $cardUser->getZip()
            ];
        }

        $params = [
            // Email expects a single email address. We don't keep track of primary addresses, so just grab the first
            'email'                => $cardUser->hasEmails() ? $cardUser->getEmails()[0] : null,
            'description'          => $cardUser->getName(),
            'opaqueDataValue'      => $token->getValue(),
            'opaqueDataDescriptor' => PaymentAdapter::PAYMENT_DESCRIPTOR,
            'billingData'          => $billingData
        ];

        /*
         * Omnipay makes multiple requests to Authorize when creating a card,
         * so detect whether customer profile already exists to prevent invalid token error
         */
        $createProfileRequest = false;
        if ($id = $cardUser->getGatewayId()) {
            $params['customerProfileId'] = $id;
            $createProfileRequest        = true;
        }

        $CIMGateway = $this->getCIMGateway();

        $CIMGateway->setApiLoginId($this->getAccountKey())
                   ->setTransactionKey($this->getAccountSecret())
                   ->setDeveloperMode($this->useDeveloperEndpoint);

        if ($createProfileRequest) {
            $response = $CIMGateway->createCardFromProfile($params)->send();
        } else {
            $response = $CIMGateway->createCard($params)->send();
        }

        return $response;
    }

    public function deleteCard(CreditCard $card): ResponseInterface
    {
        $CIMGateway = $this->getCIMGateway();

        $CIMGateway->setApiLoginId($this->getAccountKey())
                   ->setTransactionKey($this->getAccountSecret())
                   ->setDeveloperMode($this->useDeveloperEndpoint);

        return $CIMGateway->deleteCard(
            [
                'customerProfileId'        => $card->getCreditCardUser()->getGatewayId(),
                'customerPaymentProfileId' => $card->getToken()
            ]
        )->send();
    }

    public function retrieveCard(String $payerToken, String $cardToken): ResponseInterface
    {
        $CIMGateway = $this->getCIMGateway();

        $CIMGateway->setApiLoginId($this->getAccountKey())
                   ->setTransactionKey($this->getAccountSecret())
                   ->setDeveloperMode($this->useDeveloperEndpoint);

        return $CIMGateway->getPaymentProfile([
            'customerProfileId'        => $payerToken,
            'customerPaymentProfileId' => $cardToken
        ])->send();
    }

    public function getCIMGateway(): CIMGateway
    {
        return $this->CIMGateway;
    }
}
