<?php

namespace ServiceCore\PaymentGateway\CardConnect\Adapter;

use ServiceCore\PaymentGateway\CardConnect\Helper\ClientAware;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractPayment;
use ServiceCore\PaymentGateway\Core\Data\ACHToken;
use ServiceCore\PaymentGateway\Core\Data\CreditCard;
use ServiceCore\PaymentGateway\Core\Data\GatewayPayment;
use ServiceCore\PaymentGateway\Core\Data\Payment as SourcePayment;
use ServiceCore\PaymentGateway\Core\Data\PaymentData;
use ServiceCore\PaymentGateway\Core\Data\Token;

class Payment extends AbstractPayment implements ClientAwareInterface
{
    use ClientAware;

    public const SETTLEMENT_STATUS_AUTHORIZED = 'Authorized';
    public const SETTLEMENT_STATUS_QUEUED     = 'Queued for Capture';
    public const SETTLEMENT_STATUS_ACCEPTED   = 'Accepted';
    public const SETTLEMENT_STATUS_REJECTED   = 'Rejected';
    public const SETTLEMENT_STATUS_VOIDED     = 'Voided';
    public const SETTLEMENT_STATUS_DECLINED   = 'Declined';

    public const RESPONSE_AUTH_CODE_REFUNDED = 'REFUND';
    public const RESPONSE_AUTH_CODE_VOIDED   = 'REVERS';

    public function createPayment(CreditCard $card, PaymentData $data): array
    {
        return $this->getClient()->authorizeTransaction(
            [
                'merchid' => $this->client->getMerchantId(),
                'profile' => $card->getCreditCardUser()->getGatewayId() . '/' . $card->getToken(),
                'expiry'  => $card->getExpMonth() . \substr($card->getExpYear(), -2, 2),
                'amount'  => $data->getAmount(),
                'capture' => 'Y'
            ]
        );
    }

    public function createPaymentFromToken(Token $token, PaymentData $data): array
    {
        $billingData = $data->getBillingData();

        return $this->getClient()->authorizeTransaction(
            [
                'merchid'  => $this->client->getMerchantId(),
                'account'  => $token->getValue(),
                'expiry'   => $token->getExpiry(),
                'amount'   => $data->getAmount(),
                'capture'  => 'Y',
                'name'     => $data->getDescriptor(),
                'address'  => $billingData['address'] ?? '',
                'address2' => $billingData['secondaryAddress'] ?? '',
                'city'     => $billingData['city'] ?? '',
                'postal'   => $billingData['zip'] ?? '',
                'region'   => $billingData['state'] ?? '',
                'country'  => $billingData['country'] ?? ''
            ]
        );
    }

    public function createPaymentFromACHToken(ACHToken $token, PaymentData $data): array
    {
        $billingData = $data->getBillingData();

        return $this->getClient()->authorizeACHTransaction(
            [
                'merchid'  => $this->client->getACHMerchantId(),
                'account'  => $token->getValue(),
                'accttype' => $token->getAcctType(),
                'name'     => $token->getHolder(),
                'amount'   => $data->getAmount(),
                'capture'  => 'Y',
                'address'  => $billingData['address'] ?? '',
                'address2' => $billingData['secondaryAddress'] ?? '',
                'city'     => $billingData['city'] ?? '',
                'postal'   => $billingData['zip'] ?? '',
                'region'   => $billingData['state'] ?? '',
                'country'  => $billingData['country'] ?? ''
            ]
        );
    }

    public function retrievePayment(SourcePayment $payment): ?GatewayPayment
    {
        if (!$transactionId = $payment->getTransactionId()) {
            return null;
        }

        return $this->parseGatewayPayment(
            $this->getClient()->inquireTransaction($transactionId)
        );
    }

    public function voidPayment(SourcePayment $payment): array
    {
        return $this->getClient()->voidTransaction(
            [
                'merchid' => $this->client->getMerchantId(),
                'amount'  => '0',
                'retref'  => $payment->getTransactionId(),
            ]
        );
    }

    public function refundPayment(
        SourcePayment $payment,
        float $amount,
        bool $voidIfFails = false
    ): array {
        if ($voidIfFails === true) {
            $ccPayment = $this->retrievePayment($payment);

            if (!$ccPayment) {
                throw new \RuntimeException('Could not retrieve payment to refund');
            }

            $settlementStatus = $ccPayment->getTransactionStatus();
            if ($settlementStatus === GatewayPayment::STATUS_PENDING
                || $settlementStatus === GatewayPayment::STATUS_PENDING_CAPTURE
            ) {
                // payment can still be voided instead of refunded

                return $this->voidPayment($payment);
            }
        }

        return $this->getClient()->refundTransaction(
            [
                'merchid' => $this->client->getMerchantId(),
                'amount'  => $amount,
                'retref'  => $payment->getTransactionId()
            ]
        );
    }

    private function parseGatewayPayment(array $response): ?GatewayPayment
    {
        $status = $response['setlstat'] ?? null;

        if ($status === null) {
            return null;
        }

        switch ($status) {
            case self::SETTLEMENT_STATUS_QUEUED:
                $status = GatewayPayment::STATUS_PENDING;

                break;
            case self::SETTLEMENT_STATUS_AUTHORIZED:
                $status = GatewayPayment::STATUS_PENDING_CAPTURE;

                break;
            case self::SETTLEMENT_STATUS_ACCEPTED:
                $status = GatewayPayment::STATUS_SETTLED;

                break;
            case self::SETTLEMENT_STATUS_REJECTED:
            case self::SETTLEMENT_STATUS_DECLINED:
                $status = GatewayPayment::STATUS_DECLINED;

                break;
            case self::SETTLEMENT_STATUS_VOIDED:
                $status = GatewayPayment::STATUS_VOIDED;

                break;
            default:
                $status = GatewayPayment::STATUS_UNKNOWN;

                break;
        }

        $isVoidable   = null;
        $isRefundable = null;

        if (\array_key_exists('voidable', $response)) {
            $isVoidable = $response['voidable'] === 'Y';
        }

        if (\array_key_exists('refundable', $response)) {
            $isRefundable = $response['refundable'] === 'Y';
        }

        return new GatewayPayment(
            $response['retref'],
            $status,
            $response['respcode'],
            $isVoidable,
            $isRefundable
        );
    }
}
