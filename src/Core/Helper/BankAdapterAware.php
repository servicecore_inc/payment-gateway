<?php

namespace ServiceCore\PaymentGateway\Core\Helper;

use RuntimeException;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractBank;

trait BankAdapterAware
{
    /** @var AbstractBank */
    private $bankAdapter;

    public function getBankAdapter(): AbstractBank
    {
        if (!$this->bankAdapter) {
            throw new RuntimeException('Card adapter is not set');
        }

        return $this->bankAdapter;
    }

    public function setBankAdapter(AbstractBank $bankAdapter): self
    {
        $this->bankAdapter = $bankAdapter;

        return $this;
    }
}
