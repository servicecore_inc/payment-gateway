<?php

namespace ServiceCore\PaymentGateway\Test\Authorize\Adapter\Factory;

use GuzzleHttp\ClientInterface;
use Laminas\ServiceManager\ServiceManager;
use Omnipay\Common\GatewayInterface;
use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\Authorize\Adapter\Account as AccountAdapter;
use ServiceCore\PaymentGateway\Authorize\Adapter\Factory\Account as AccountAdapterFactory;

/**
 * @group factory
 * @group account
 * @group account-factory
 */
class AccountTest extends TestCase
{
    public function testInvoke(): void
    {
        $factory   = new AccountAdapterFactory();
        $container = new ServiceManager();
        $class     = $factory($container, AccountAdapter::class);

        $this->assertInstanceOf(ClientInterface::class, $class->getHttpClient());
        $this->assertInstanceOf(GatewayInterface::class, $class->getAIMGateway());
    }
}
