<?php

namespace ServiceCore\PaymentGateway\Core\Data;

class BillingAddress
{
    /**
     * @var string|null
     */
    private $address;

    /**
     * @var string|null
     */
    private $city;

    /**
     * @var string|null
     */
    private $state;

    /**
     * @var string|null
     */
    private $zip;

    public function __construct(
        ?string $address = null,
        ?string $city = null,
        ?string $state = null,
        ?string $zip = null
    ) {
        $this->address = $address;
        $this->city    = $city;
        $this->state   = $state;
        $this->zip     = $zip;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }
}
