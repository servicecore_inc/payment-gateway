<?php

namespace ServiceCore\PaymentGateway\Authorize\Adapter\Factory\Delegator;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use ServiceCore\PaymentGateway\Authorize\Adapter\Account as AccountAdapter;

class AccountDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        $context = $callback();

        if (\method_exists($context, 'setAdapter')) {
            $adapter = $container->get(AccountAdapter::class);

            $context->setAdapter($adapter);
        }

        return $context;
    }
}
