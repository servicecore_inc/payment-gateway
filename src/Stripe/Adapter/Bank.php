<?php

namespace ServiceCore\PaymentGateway\Stripe\Adapter;

use ServiceCore\PaymentGateway\Core\Adapter\AbstractBank;
use ServiceCore\PaymentGateway\Core\Data\BankAccount;
use ServiceCore\PaymentGateway\Core\Data\GatewayAccount;
use ServiceCore\PaymentGateway\Core\Data\Token;
use Stripe\Account as StripeAccount;
use Stripe\BankAccount as StripeBank;

class Bank extends AbstractBank
{
    public function createBank(GatewayAccount $account, Token $token): StripeBank
    {
        $stripeAccount = $this->retrieveAccount($account->getAccountId());

        return $stripeAccount->external_accounts->create([
            'external_account' => $token->getValue()
        ]);
    }

    public function deleteBank(BankAccount $bank): StripeBank
    {
        $stripeAccount = $this->retrieveAccount($bank->getGatewayAccount()->getAccountId());

        return $stripeAccount->external_accounts->retrieve($bank->getToken())->delete();
    }

    private function retrieveAccount(string $id): StripeAccount
    {
        return StripeAccount::retrieve($id);
    }
}
