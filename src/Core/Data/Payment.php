<?php

namespace ServiceCore\PaymentGateway\Core\Data;

interface Payment
{
    public function getLastFour(): ?string;

    public function getExpiry(): ?string;

    public function getTransactionId(): ?string;

    public function getAmount();
}
