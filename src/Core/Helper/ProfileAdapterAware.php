<?php

namespace ServiceCore\PaymentGateway\Core\Helper;

use RuntimeException;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractProfile;

trait ProfileAdapterAware
{
    /** @var AbstractProfile */
    private $profileAdapter;

    public function getProfileAdapter(): AbstractProfile
    {
        if (!$this->profileAdapter) {
            throw new RuntimeException('Profile adapter is not set');
        }

        return $this->profileAdapter;
    }

    public function setProfileAdapter(AbstractProfile $profileAdapter): self
    {
        $this->profileAdapter = $profileAdapter;

        return $this;
    }
}
