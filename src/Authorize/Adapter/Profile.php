<?php

namespace ServiceCore\PaymentGateway\Authorize\Adapter;

use Omnipay\Common\Message\ResponseInterface;
use ServiceCore\PaymentGateway\Authorize\Gateway\CIMGateway;
use ServiceCore\PaymentGateway\Authorize\Helper\DeveloperEndpointAware;
use ServiceCore\PaymentGateway\Authorize\Helper\GatewayCredentialAware;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractProfile;

class Profile extends AbstractProfile
{
    use DeveloperEndpointAware;
    use GatewayCredentialAware;

    private CIMGateway $CIMGateway;

    public function __construct(CIMGateway $CIMGateway)
    {
        $this->CIMGateway = $CIMGateway;
    }

    public function retrieveProfile(string $profileToken): ResponseInterface
    {
        $this->CIMGateway->setApiLoginId($this->getAccountKey())
                         ->setTransactionKey($this->getAccountSecret())
                         ->setDeveloperMode($this->useDeveloperEndpoint);

        return $this->CIMGateway->getCustomerProfile(
            [
                'customerProfileId' => $profileToken
            ]
        )->send();
    }
}
