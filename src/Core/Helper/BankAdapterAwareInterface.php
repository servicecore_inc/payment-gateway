<?php

namespace ServiceCore\PaymentGateway\Core\Helper;

use ServiceCore\PaymentGateway\Core\Adapter\AbstractBank;

interface BankAdapterAwareInterface
{
    public function getBankAdapter(): AbstractBank;

    public function setBankAdapter(AbstractBank $bankAdapter);
}
