<?php

namespace ServiceCore\PaymentGateway\Stripe\Event\Charge;

use ServiceCore\PaymentGateway\Core\Event\EventInterface;
use Stripe\Event;

class Failed extends Event implements EventInterface
{

}
