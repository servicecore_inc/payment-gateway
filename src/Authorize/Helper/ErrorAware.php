<?php

namespace ServiceCore\PaymentGateway\Authorize\Helper;

use Omnipay\AuthorizeNet\Message\AIMResponse;
use Omnipay\AuthorizeNet\Message\CIMResponse;
use ZF\ApiProblem\ApiProblem;

trait ErrorAware
{
    /** @var AIMResponse|CIMResponse|null */
    private $authorizeResponse;

    public function hasAuthorizeError($response = null): bool
    {
        if (!$response) {
            $this->authorizeResponse = $response;

            return true;
        }

        if (!$response->isSuccessful()) {
            $this->authorizeResponse = $response;

            return true;
        }

        $errorCodes = [3, 4, 'Error'];

        if (\in_array($response->getResultCode(), $errorCodes, false)) {
            $this->authorizeResponse = $response;

            return true;
        }

        return false;
    }

    public function getAuthorizeError(): ?ApiProblem
    {
        if ($this->authorizeResponse) {
            return new ApiProblem(422, $this->authorizeResponse->getMessage());
        }

        return null;
    }
}
