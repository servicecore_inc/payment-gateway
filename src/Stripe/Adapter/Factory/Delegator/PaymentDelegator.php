<?php

namespace ServiceCore\PaymentGateway\Stripe\Adapter\Factory\Delegator;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use ServiceCore\PaymentGateway\Stripe\Adapter\Payment as PaymentAdapter;

class PaymentDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        $context = $callback();

        if (\method_exists($context, 'setAdapter')) {
            /** @var PaymentAdapter $adapter */
            $adapter = $container->get(PaymentAdapter::class);

            $context->setAdapter($adapter);
        }

        return $context;
    }
}
