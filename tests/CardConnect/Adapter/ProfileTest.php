<?php

namespace ServiceCore\PaymentGateway\Test\CardConnect\Adapter;

use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\CardConnect\Adapter\Profile;
use ServiceCore\PaymentGateway\CardConnect\Service\CardConnect;

/**
 * @group authorize
 * @group profile
 * @group profile-authorize
 */
class ProfileTest extends TestCase
{
    public function testRetrieveProfile(): void
    {
        $adapter              = new Profile();
        $client               = $this->createStub(CardConnect::class);
        $expectedProfileToken = 'fooBar';
        $response             = ['success'];

        $client->expects($this->once())
               ->method('profileGet')
               ->with($expectedProfileToken)
               ->willReturn($response);

        $adapter->setClient($client);

        $this->assertSame($response, $adapter->retrieveProfile($expectedProfileToken));
    }
}
