<?php

namespace ServiceCore\PaymentGateway\Authorize\Helper;

use SimpleXMLElement;

trait BillingAware
{
    /** @var array */
    private $billingData;

    public function setBillingInfo(SimpleXMLElement $billTo): SimpleXMLElement
    {
        if ($this->billingData) {
            $billTo->firstName = $this->billingData['firstName'] ?? null;
            $billTo->lastName  = $this->billingData['lastName'] ?? null;
            $billTo->address   = $this->billingData['address'] ?? null;
            $billTo->city      = $this->billingData['city'] ?? null;
            $billTo->state     = $this->billingData['state'] ?? null;
            $billTo->zip       = $this->billingData['zip'] ?? null;
        }

        return $billTo;
    }

    public function setBillingData(?array $billingData): self
    {
        $this->billingData = $billingData;

        return $this;
    }
}
