<?php

namespace ServiceCore\PaymentGateway\Core\Data;

interface BankAccount
{
    public function getToken();

    public function getGatewayAccount(): GatewayAccount;
}
