<?php

namespace ServiceCore\PaymentGateway\CardConnect\Adapter;

use ServiceCore\PaymentGateway\CardConnect\Helper\ClientAware;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractProfile;

class Profile extends AbstractProfile implements ClientAwareInterface
{
    use ClientAware;

    public function retrieveProfile(string $profileToken): array
    {
        return $this->client->profileGet($profileToken);
    }
}
