<?php

namespace ServiceCore\PaymentGateway\Core\Adapter;

abstract class AbstractAccount
{
    abstract public function createAccount(array $params, array $options = []);

    abstract public function retrieveAccount(string $id);

    abstract public function updateAccount(string $id, array $params);

    abstract public function deleteAccount(string $id);
}
