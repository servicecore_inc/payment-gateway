<?php

namespace ServiceCore\PaymentGateway\Stripe\Adapter;

use ServiceCore\PaymentGateway\Core\Adapter\AbstractCard;
use ServiceCore\PaymentGateway\Core\Data\BillingAddress;
use ServiceCore\PaymentGateway\Core\Data\CreditCard;
use ServiceCore\PaymentGateway\Core\Data\CreditCardUser;
use ServiceCore\PaymentGateway\Core\Data\Token;
use Stripe\Customer as StripeCustomer;
use Stripe\Event as StripeEvent;
use Stripe\StripeObject;

class Card extends AbstractCard
{
    public function createCard(
        CreditCardUser $cardUser,
        Token $token,
        ?BillingAddress $billingAddress = null
    ): StripeObject {
        if ($cardUser->getGatewayId()) {
            $stripeCustomer = $this->retrieveCustomer($cardUser);
        } else {
            $stripeCustomer = $this->createCustomer($cardUser);
        }
        unset($billingAddress);

        return $stripeCustomer->sources->create(['source' => $token->getValue()]);
    }

    public function retrieveCard(string $payerToken, string $cardToken): StripeObject
    {
        $stripeCustomer = StripeCustomer::retrieve($payerToken);

        return $stripeCustomer->sources->retrieve($cardToken);
    }

    public function createCustomer(CreditCardUser $ccEntity): StripeCustomer
    {
        return StripeCustomer::create(
            [
                'description' => \sprintf('%s %s', $ccEntity->getFirstName(), $ccEntity->getLastName()),
            ],
            [
                'stripe_account' => $ccEntity->getGatewayAccountId()
            ]
        );
    }

    public function retrieveCustomer(CreditCardUser $ccEntity): StripeCustomer
    {
        return StripeCustomer::retrieve(
            $ccEntity->getGatewayId(),
            [
                'stripe_account' => $ccEntity->getGatewayAccountId()
            ]
        );
    }

    public function deleteCard(CreditCard $card): void
    {
        $this->retrieveCard($card->getCreditCardUser()->getGatewayId(), $card->getToken())->delete();
    }

    public function retrieveEvent(string $id): StripeEvent
    {
        return StripeEvent::retrieve($id);
    }
}
