<?php

namespace ServiceCore\PaymentGateway\Stripe\Adapter;

use ServiceCore\PaymentGateway\Core\Adapter\AbstractPayment;
use ServiceCore\PaymentGateway\Core\Data\CreditCard;
use ServiceCore\PaymentGateway\Core\Data\CreditCardUser;
use ServiceCore\PaymentGateway\Core\Data\Payment as GatewayPayment;
use ServiceCore\PaymentGateway\Core\Data\PaymentData;
use ServiceCore\PaymentGateway\Core\Data\Token;
use Stripe\Charge as StripeCharge;
use Stripe\Customer as StripeCustomer;
use Stripe\Event as StripeEvent;
use Stripe\Refund;

class Payment extends AbstractPayment
{
    public function createPayment(CreditCard $card, PaymentData $data): StripeCharge
    {
        return StripeCharge::create([], []);
    }

    public function createPaymentFromToken(Token $token, PaymentData $data): StripeCharge
    {
        $params = [
            'amount'               => $data->getAmount(),
            'statement_descriptor' => $data->getDescriptor(),
            'source'               => $token->getValue()
        ];

        return StripeCharge::create($params);
    }

    public function retrievePayment(GatewayPayment $payment, array $options = null): StripeCharge
    {
        return StripeCharge::retrieve($payment->getTransactionId(), $options);
    }

    public function refundPayment(
        GatewayPayment $payment,
        float $amount,
        bool $voidIfFails = false
    ): Refund {
        return Refund::create(
            [
                'charge' => $payment->getTransactionId(),
                'amount' => $amount,
                'reason' => 'refunded'
            ]
        );
    }

    public function voidPayment(GatewayPayment $payment): void
    {
        // TODO: Implement voidPayment() method.
    }

    public function createCustomer(CreditCardUser $ccEntity, string $accountId): StripeCustomer
    {
        return StripeCustomer::create(
            [
                'description' => \sprintf('%s %s', $ccEntity->getFirstName(), $ccEntity->getLastName()),
            ],
            [
                'stripe_account' => $accountId
            ]
        );
    }

    public function retrieveCustomer(CreditCardUser $ccEntity, string $accountId): StripeCustomer
    {
        return StripeCustomer::retrieve(
            $ccEntity->getGatewayId(),
            [
                'stripe_account' => $accountId
            ]
        );
    }

    public function retrieveEvent(string $id): StripeEvent
    {
        return StripeEvent::retrieve($id);
    }
}
