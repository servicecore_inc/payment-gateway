<?php

namespace ServiceCore\PaymentGateway\Authorize\Message;

use Omnipay\AuthorizeNet\Message\CIMCreatePaymentProfileRequest as CreatePaymentProfile;
use Omnipay\AuthorizeNet\Message\CIMCreatePaymentProfileResponse;
use ServiceCore\PaymentGateway\Authorize\Helper\BillingAware;
use SimpleXMLElement;

class CIMCreatePaymentProfileRequest extends CreatePaymentProfile
{
    use BillingAware;

    public function getData(): SimpleXMLElement
    {
        $data                    = $this->getBaseData();
        $data->customerProfileId = $this->getCustomerProfileId();

        $paymentProfile = $data->addChild('paymentProfile');

        $this->setBillingInfo($paymentProfile->addChild('billTo'));

        $opaque = $paymentProfile->addChild('payment')->addChild('opaqueData');

        $opaque->dataDescriptor = $this->getParameter('opaqueDataDescriptor');
        $opaque->dataValue      = $this->getParameter('opaqueDataValue');

        $this->addTransactionSettings($data);

        return $data;
    }

    public function sendData($data)
    {
        $headers      = ['Content-Type' => 'text/xml; charset=utf-8'];
        $data         = $data->saveXml();
        $httpResponse = $this->httpClient->request('POST', $this->getEndpoint(), $headers, $data);

        $response = new CIMCreatePaymentProfileResponse($this, $httpResponse->getBody());

        if ($response->isSuccessful()) {
            $parameters = [
                'customerProfileId'        => $response->getCustomerProfileId(),
                'customerPaymentProfileId' => $response->getCustomerPaymentProfileId(),
            ];
            // Get the payment profile for the specified card.
            $response = $this->makeGetPaymentProfileRequest($parameters);
        }

        return $this->response = $response;
    }
}
