<?php

namespace ServiceCore\PaymentGateway\Authorize\Message;

use Omnipay\AuthorizeNet\Message\CIMCreateCardRequest as CardRequest;
use Omnipay\AuthorizeNet\Message\CIMCreatePaymentProfileResponse;
use ServiceCore\PaymentGateway\Authorize\Helper\BillingAware;
use SimpleXMLElement;

class CIMCreateCardRequest extends CardRequest
{
    use BillingAware;

    public function getData(): SimpleXMLElement
    {
        $this->validate('opaqueDataValue', 'opaqueDataDescriptor');

        $data = $this->getBaseData();

        $this->addProfileData($data);
        $this->addTransactionSettings($data);

        $paymentProfiles = $data->profile->paymentProfiles;

        $this->setBillingInfo($paymentProfiles->addChild('billTo'));

        $opaque = $paymentProfiles->addChild('payment')->addChild('opaqueData');

        $opaque->dataDescriptor = $this->getParameter('opaqueDataDescriptor');
        $opaque->dataValue      = $this->getParameter('opaqueDataValue');

        return $data;
    }

    public function makeCreatePaymentProfileRequest($parameters): CIMCreatePaymentProfileResponse
    {
        $obj = new CIMCreatePaymentProfileRequest($this->httpClient, $this->httpRequest);

        $obj->initialize(\array_replace($this->getParameters(), $parameters));

        return $obj->send();
    }
}
