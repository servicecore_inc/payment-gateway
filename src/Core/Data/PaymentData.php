<?php

namespace ServiceCore\PaymentGateway\Core\Data;

class PaymentData
{
    private float   $amount;
    private ?string $descriptor;
    private ?string $identifier;
    private ?array  $billingData;

    public function __construct(
        float $amount,
        ?string $descriptor = null,
        ?string $identifier = null,
        ?array $billingData = null
    ) {
        $this->amount      = $amount;
        $this->descriptor  = $descriptor;
        $this->identifier  = $identifier;
        $this->billingData = $billingData;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getDescriptor(): ?string
    {
        return $this->descriptor;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * $billingData standard keys: firstName, lastName, address, secondaryAddress, city, state, zip
     */
    public function getBillingData(): ?array
    {
        return $this->billingData;
    }
}
