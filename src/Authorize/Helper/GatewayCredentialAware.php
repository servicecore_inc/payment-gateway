<?php

namespace ServiceCore\PaymentGateway\Authorize\Helper;

trait GatewayCredentialAware
{
    /** @var string */
    private $accountKey = '';

    /** @var string */
    private $accountSecret = '';

    public function getAccountKey(): string
    {
        return $this->accountKey;
    }

    public function setAccountKey(string $accountKey): self
    {
        $this->accountKey = $accountKey;

        return $this;
    }

    public function getAccountSecret(): string
    {
        return $this->accountSecret;
    }

    public function setAccountSecret(string $accountSecret): self
    {
        $this->accountSecret = $accountSecret;

        return $this;
    }
}
