<?php

use Laminas\ServiceManager\Factory\InvokableFactory;
use ServiceCore\PaymentGateway\Authorize\Adapter\Account as AuthorizeAccountAdapter;
use ServiceCore\PaymentGateway\Authorize\Adapter\Card as AuthorizeCardAdapter;
use ServiceCore\PaymentGateway\Authorize\Adapter\Factory\Account as AuthorizeAccountAdapterFactory;
use ServiceCore\PaymentGateway\Authorize\Adapter\Factory\Card as AuthorizeCardAdapterFactory;
use ServiceCore\PaymentGateway\Authorize\Adapter\Factory\Payment as AuthorizePaymentAdapterFactory;
use ServiceCore\PaymentGateway\Authorize\Adapter\Factory\Profile as AuthorizeProfileAdapterFactory;
use ServiceCore\PaymentGateway\Authorize\Adapter\Payment as AuthorizePaymentAdapter;
use ServiceCore\PaymentGateway\Authorize\Adapter\Profile as AuthorizeProfileAdapter;
use ServiceCore\PaymentGateway\CardConnect\Adapter\Account as CardConnectAccountAdapter;
use ServiceCore\PaymentGateway\CardConnect\Adapter\Card as CardConnectCardAdapter;
use ServiceCore\PaymentGateway\CardConnect\Adapter\Payment as CardConnectPaymentAdapter;
use ServiceCore\PaymentGateway\CardConnect\Adapter\Profile as CardConnectProfileAdapter;
use ServiceCore\PaymentGateway\CardConnect\Service\CardConnect as CardConnectService;
use ServiceCore\PaymentGateway\CardConnect\Service\Factory\CardConnect as CardConnectServiceFactory;
use ServiceCore\PaymentGateway\Stripe\Adapter\Account as StripeAccountAdapter;
use ServiceCore\PaymentGateway\Stripe\Adapter\Bank as StripeBankAdapter;
use ServiceCore\PaymentGateway\Stripe\Adapter\Card as StripeCardAdapter;
use ServiceCore\PaymentGateway\Stripe\Adapter\Config as StripeConfigAdapter;
use ServiceCore\PaymentGateway\Stripe\Adapter\Factory\Config as StripeConfigAdapterFactory;
use ServiceCore\PaymentGateway\Stripe\Adapter\Factory\Delegator\ConfigDelegator as StripeConfigDelegator;
use ServiceCore\PaymentGateway\Stripe\Adapter\Payment as StripePaymentAdapter;

$authorizeSmConfig = [
    'factories' => [
        AuthorizeAccountAdapter::class => AuthorizeAccountAdapterFactory::class,
        AuthorizeCardAdapter::class    => AuthorizeCardAdapterFactory::class,
        AuthorizeProfileAdapter::class => AuthorizeProfileAdapterFactory::class,
        AuthorizePaymentAdapter::class => AuthorizePaymentAdapterFactory::class,
    ],
];

$cardConnectSmConfig = [
    'factories' => [
        CardConnectAccountAdapter::class => InvokableFactory::class,
        CardConnectCardAdapter::class    => InvokableFactory::class,
        CardConnectPaymentAdapter::class => InvokableFactory::class,
        CardConnectProfileAdapter::class => InvokableFactory::class,
        CardConnectService::class        => CardConnectServiceFactory::class
    ],
];

$stripeSmConfig = [
    'factories'  => [
        StripeAccountAdapter::class => InvokableFactory::class,
        StripeCardAdapter::class    => InvokableFactory::class,
        StripePaymentAdapter::class => InvokableFactory::class,
        StripeConfigAdapter::class  => StripeConfigAdapterFactory::class,
    ],
    'delegators' => [
        StripeAccountAdapter::class => [
            StripeConfigDelegator::class
        ],
        StripeCardAdapter::class    => [
            StripeConfigDelegator::class
        ],
        StripePaymentAdapter::class => [
            StripeConfigDelegator::class
        ],
        StripeBankAdapter::class    => [
            StripeConfigDelegator::class
        ],
    ]
];

return [
    'payment_gateway' => [
        'service_manager' => \array_merge_recursive(
            $authorizeSmConfig,
            $cardConnectSmConfig,
            $stripeSmConfig
        ),
    ],
];
