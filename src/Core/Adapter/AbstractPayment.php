<?php

namespace ServiceCore\PaymentGateway\Core\Adapter;

use ServiceCore\PaymentGateway\Core\Data\CreditCard;
use ServiceCore\PaymentGateway\Core\Data\Payment;
use ServiceCore\PaymentGateway\Core\Data\PaymentData;
use ServiceCore\PaymentGateway\Core\Data\Token;

abstract class AbstractPayment
{
    abstract public function createPayment(CreditCard $card, PaymentData $data);

    abstract public function createPaymentFromToken(Token $token, PaymentData $data);

    abstract public function retrievePayment(Payment $payment);

    abstract public function voidPayment(Payment $payment);

    abstract public function refundPayment(Payment $payment, float $amount, bool $voidIfFails = false);
}
