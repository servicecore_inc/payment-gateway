<?php

namespace PaymentGateway\CardConnect\Adapter\Delegator;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use ServiceCore\PaymentGateway\CardConnect\Adapter\Card as CardAdapter;

class CardDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        $context = $callback();

        if (\method_exists($context, 'setAdapter')) {
            $adapter = $container->get(CardAdapter::class);

            $context->setAdapter($adapter);
        }

        return $context;
    }
}
