# CHANGELOG

All notable changes to this project will be documented in this file, in reverse chronological order by release.
This project does follow [Semantic Versioning](semver.org).

## 5.4.0

### Added

- Add billing address fields for CardConnect ACH payments.

## 5.3.0

### Added

- GatewayPayment objects will now have `isVoidable` and `isRefundable` keys set with boolean values if
  those properties can be determined. Otherwise, they are null. Initially supported on CardConnect.

## 5.2.0

### Added

- Add a method to submit authorization requests for CardConnect ACH payments. Adds client properties and methods
  for using a different set of credentials specifically for ACH.

## 5.1.0

### Changed

- Remove expiry from updateCard() profileCreate() params to allow setting default on cards where that info
  is not available (when cards are retrieved from a CardConnect getProfile response, for example)

## 5.0.3

### Changed

- Failed json_decode of CardConnect response now returns identifiable Error key in response array.

### Added

- Public RESPONSE_ERROR variable added to encompass more detailed error message for malformed responses.

## 5.0.2

### Fixed

- Card/clover connect responses were not properly decoded if Transfer-Encoding header was present.

## 5.0.1

### Updated

- Updated guzzle6 adapter to guzzle7.

## 5.x.x

### Removed

- Method signature for the card adapter `ServiceCore\PaymentGateway\Core\Adapter\AbstractCard::retrieveCard()` changed
  to take strings of the foreign "token" identifiers for both foreign customer and foreign card, instead of the CardUser
  interface.
- CardConnect service method `ServiceCore\PaymentGateway\CardConnect\Service::profileGet(string, string)` modified to
  match CardConnect naming conventions by splitting concerns to `profileGet(string)` and `accountGet(string, string)`.
  Currently, `profileGet()` takes a customer identifier and a card identifiers to return a card. However, in CardConnect
  a card is referred to as an `account` while a customer entity is a `profile`.

### Added

- Added AbstractProfile adapter and implemented in Authorize and CardConnect. This adapter can be used to retrieve
  customer entities. 
