<?php

namespace ServiceCore\PaymentGateway\Core\Exception;

use Exception;

class MissingTransactionId extends Exception
{
    public function __construct()
    {
        parent::__construct('Payment provided does not have a transaction ID');
    }
}
