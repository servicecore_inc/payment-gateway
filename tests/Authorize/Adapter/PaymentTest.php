<?php

namespace ServiceCore\PaymentGateway\Test\Authorize\Adapter;

use Omnipay\AuthorizeNet\Message\CIMPurchaseRequest;
use Omnipay\AuthorizeNet\Message\CIMResponse;
use Omnipay\AuthorizeNet\Model\CardReference;
use Omnipay\AuthorizeNet\Model\TransactionReference;
use Omnipay\Common\GatewayInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\Authorize\Adapter\Payment as PaymentAdapter;
use ServiceCore\PaymentGateway\Authorize\Gateway\AIMGateway;
use ServiceCore\PaymentGateway\Authorize\Gateway\CIMGateway;
use ServiceCore\PaymentGateway\Core\Data\PaymentData;
use ServiceCore\PaymentGateway\Core\Data\Token;
use ServiceCore\PaymentGateway\Test\Support\MockCreditCard;
use ServiceCore\PaymentGateway\Test\Support\MockPayment;

/**
 * @group authorize
 * @group payment
 * @group payment-authorize
 */
class PaymentTest extends TestCase
{
    public function testCreatePayment(): void
    {
        $AIMGateway = $this->getMockAIMGateway();
        $CIMGateway = $this->getMockCIMGateway();
        $adapter    = new PaymentAdapter($AIMGateway, $CIMGateway);
        $card       = new MockCreditCard();
        $request    = $this->createStub(CIMPurchaseRequest::class);
        $response   = $this->createStub(CIMResponse::class);
        $amount     = 1.0;
        $invoiceNo  = '1';

        $billingData = [
            'firstName' => 'tester',
            'lastName'  => 'son',
            'address'   => '1111 Test St',
            'city'      => 'TestVille',
            'state'     => 'CO',
            'zip'       => 80221
        ];

        $request->expects(self::once())
                ->method('send')
                ->willReturn($response);

        $purchaseParams = [
            'cardReference' => new CardReference(),
            'amount'        => $amount,
            'billingData'   => $billingData,
            'invoiceNumber' => $invoiceNo
        ];

        $CIMGateway->expects(self::once())
                   ->method('purchase')
                   ->with($purchaseParams)
                   ->willReturn($request);

        $AIMGateway->expects(self::never())
                   ->method('purchase');

        $paymentData = new PaymentData($amount, null, $invoiceNo, $billingData);

        $adapter->createPayment($card, $paymentData);
    }

    public function testCreatePaymentFromToken(): void
    {
        $AIMGateway = $this->getMockAIMGateway();
        $CIMGateway = $this->getMockCIMGateway();
        $adapter    = new PaymentAdapter($AIMGateway, $CIMGateway);
        $tokenValue = 'abc123';
        $descriptor = 'abc';
        $token      = new Token($tokenValue);
        $request    = $this->createStub(CIMPurchaseRequest::class);
        $response   = $this->createStub(CIMResponse::class);
        $amount     = 1.0;
        $invoiceNo  = '1';

        $request->expects(self::once())
                ->method('send')
                ->willReturn($response);

        $billingData = [
            'firstName' => 'tester',
            'lastName'  => 'son',
            'address'   => '1111 Test St',
            'city'      => 'TestVille',
            'state'     => 'CO',
            'zip'       => 80221
        ];

        $purchaseParams = [
            'opaqueDataDescriptor' => $descriptor,
            'opaqueDataValue'      => $tokenValue,
            'amount'               => $amount,
            'billingData'          => $billingData,
            'invoiceNumber'        => $invoiceNo
        ];

        $AIMGateway->expects(self::once())
                   ->method('purchase')
                   ->with($purchaseParams)
                   ->willReturn($request);

        $CIMGateway->expects(self::never())
                   ->method('purchase');

        $paymentData = new PaymentData($amount, $descriptor, $invoiceNo, $billingData);

        $adapter->createPaymentFromToken($token, $paymentData);
    }

    public function testVoidPayment(): void
    {
        $AIMGateway = $this->getMockAIMGateway();
        $CIMGateway = $this->getMockCIMGateway();
        $adapter    = new PaymentAdapter($AIMGateway, $CIMGateway);
        $id         = 1;
        $payment    = new MockPayment(null, $id);
        $request    = $this->createStub(CIMPurchaseRequest::class);
        $response   = $this->createStub(CIMResponse::class);

        $request->expects(self::once())
                ->method('send')
                ->willReturn($response);

        $voidParams = [
            'transactionReference' => $id
        ];

        $AIMGateway->expects(self::once())
                   ->method('void')
                   ->with($voidParams)
                   ->willReturn($request);

        $CIMGateway->expects(self::never())
                   ->method('void');

        $adapter->voidPayment($payment);
    }

    public function testRefundPayment(): void
    {
        $AIMGateway = $this->getMockAIMGateway();
        $CIMGateway = $this->getMockCIMGateway();
        $adapter    = new PaymentAdapter($AIMGateway, $CIMGateway);
        $amount     = 100;
        $request    = $this->createStub(CIMPurchaseRequest::class);
        $response   = $this->createStub(CIMResponse::class);

        $request->expects(self::once())
                ->method('send')
                ->willReturn($response);

        $cardNumber    = '3232';
        $expiry        = '1230';
        $transactionId = 'abc123';

        $reference = new TransactionReference();
        $reference->setCard(['number' => $cardNumber, 'expiry' => $expiry]);
        $reference->setTransId($transactionId);

        $refundParams = [
            'transactionReference' => $reference,
            'amount'               => $amount,
            'voidIfRefundFails'    => false
        ];

        $AIMGateway->expects(self::once())
                   ->method('refund')
                   ->with($refundParams)
                   ->willReturn($request);

        $CIMGateway->expects(self::never())
                   ->method('refund');

        $payment = new MockPayment($cardNumber, $transactionId, null, $expiry);

        $adapter->refundPayment($payment, $amount);
    }

    /**
     * @return AIMGateway|MockObject
     */
    private function getMockAIMGateway(): GatewayInterface
    {
        $gateway = $this->createStub(AIMGateway::class);

        return $this->configureGateway($gateway);
    }

    /**
     * @return CIMGateway|MockObject
     */
    private function getMockCIMGateway(): GatewayInterface
    {
        $gateway = $this->createStub(CIMGateway::class);

        return $this->configureGateway($gateway);
    }

    /**
     * @param GatewayInterface|MockObject $gateway
     * @return GatewayInterface|MockObject
     */
    private function configureGateway(GatewayInterface $gateway): GatewayInterface
    {
        $gateway->method('setTransactionKey')
                ->willReturnSelf();

        $gateway->method('setApiLoginId')
                ->willReturnSelf();

        $gateway->method('setDeveloperMode')
                ->willReturnSelf();

        return $gateway;
    }
}
