<?php

namespace ServiceCore\PaymentGateway\Stripe\Event\Account;

use ServiceCore\PaymentGateway\Core\Event\EventInterface;
use Stripe\Event;

class Updated extends Event implements EventInterface
{

}
