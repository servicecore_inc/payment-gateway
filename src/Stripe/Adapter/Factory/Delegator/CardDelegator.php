<?php

namespace ServiceCore\PaymentGateway\Stripe\Adapter\Factory\Delegator;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use ServiceCore\PaymentGateway\Stripe\Adapter\Card as CardAdapter;

class CardDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        $context = $callback();

        if (\method_exists($context, 'setAdapter')) {
            /** @var CardAdapter $adapter */
            $adapter = $container->get(CardAdapter::class);

            $context->setAdapter($adapter);
        }

        return $context;
    }
}
