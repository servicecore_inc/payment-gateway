<?php

namespace ServiceCore\PaymentGateway\Test\Authorize\Adapter;

use Omnipay\Common\Message\RequestInterface;
use Omnipay\Common\Message\ResponseInterface;
use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\Authorize\Adapter\Profile;
use ServiceCore\PaymentGateway\Authorize\Gateway\CIMGateway;

/**
 * @group authorize
 * @group profile
 * @group profile-authorize
 */
class ProfileTest extends TestCase
{
    public function testRetrieveProfile(): void
    {
        $cimGateway             = $this->createStub(CIMGateway::class);
        $request                = $this->createStub(RequestInterface::class);
        $response               = $this->createStub(ResponseInterface::class);
        $expectedAccountId      = 'foo';
        $expectedTransactionKey = 'bar';

        $request->expects($this->once())
                ->method('send')
                ->willReturn($response);

        $cimGateway->expects($this->once())
                   ->method('setApiLoginId')
                   ->with($expectedAccountId)
                   ->willReturnSelf();

        $cimGateway->expects($this->once())
                   ->method('setTransactionKey')
                   ->with($expectedTransactionKey)
                   ->willReturnSelf();

        $cimGateway->expects($this->once())
                   ->method('setDeveloperMode')
                   ->with(true)
                   ->willReturnSelf();

        $cimGateway->expects($this->once())
                   ->method('getCustomerProfile')
                   ->willReturn($request);

        $adapter = new Profile($cimGateway);

        $adapter->setAccountKey($expectedAccountId)
                ->setAccountSecret($expectedTransactionKey)
                ->setUseDeveloperEndpoint(true);

        $adapter->retrieveProfile('fooBar');
    }
}
