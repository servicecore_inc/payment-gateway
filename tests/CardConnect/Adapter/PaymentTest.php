<?php

namespace ServiceCore\PaymentGateway\Test\CardConnect\Adapter;

use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\CardConnect\Adapter\Payment as PaymentAdapter;
use ServiceCore\PaymentGateway\CardConnect\Service\CardConnect;
use ServiceCore\PaymentGateway\Core\Data\ACHToken;
use ServiceCore\PaymentGateway\Core\Data\PaymentData;
use ServiceCore\PaymentGateway\Core\Data\Token;
use ServiceCore\PaymentGateway\Test\Support\MockCreditCard;
use ServiceCore\PaymentGateway\Test\Support\MockCreditCardUser;
use ServiceCore\PaymentGateway\Test\Support\MockPayment;

/**
 * @group card-connect
 * @group payment
 * @group payment-card-connect
 */
class PaymentTest extends TestCase
{
    public function testCreatePayment(): void
    {
        $adapter = new PaymentAdapter();
        $card    = new MockCreditCard(new MockCreditCardUser(1), 'ABC123', '02', '22');
        $amount  = 100;
        $merchId = 'A100';
        $client  = $this->createStub(CardConnect::class);

        $expectedParams = [
            'merchid' => $merchId,
            'profile' => $card->getCreditCardUser()->getGatewayId() . '/' . $card->getToken(),
            'expiry'  => $card->getExpMonth() . $card->getExpYear(),
            'amount'  => $amount,
            'capture' => 'Y'
        ];

        $client->expects(self::once())
               ->method('authorizeTransaction')
               ->with($expectedParams)
               ->willReturn([]);

        $client->expects(self::once())
               ->method('getMerchantId')
               ->willReturn($merchId);

        $adapter->setClient($client);

        $adapter->createPayment($card, new PaymentData($amount));
    }

    public function testCreatePaymentFromToken(): void
    {
        $adapter     = new PaymentAdapter();
        $tokenValue  = 'ABC123';
        $merchId     = '123123';
        $amount      = 100;
        $tokenExpiry = '0222';
        $token       = $this->createStub(Token::class);

        $token->expects(self::once())
              ->method('getValue')
              ->willReturn($tokenValue);

        $token->expects(self::once())
              ->method('getExpiry')
              ->willReturn($tokenExpiry);

        $client         = $this->createStub(CardConnect::class);
        $expectedParams = [
            'merchid'  => $merchId,
            'account'  => $tokenValue,
            'expiry'   => $tokenExpiry,
            'amount'   => $amount,
            'capture'  => 'Y',
            'name'     => '',
            'address'  => '',
            'address2' => '',
            'city'     => '',
            'postal'   => '',
            'region'   => '',
            'country'  => ''
        ];

        $client->expects(self::once())
               ->method('authorizeTransaction')
               ->with($expectedParams)
               ->willReturn([]);

        $client->expects(self::once())
               ->method('getMerchantId')
               ->willReturn($merchId);

        $adapter->setClient($client);

        $adapter->createPaymentFromToken($token, new PaymentData($amount));
    }

    public function testCreatePaymentFromACHToken(): void
    {
        $adapter    = new PaymentAdapter();
        $tokenValue = 'ABC123';
        $merchId    = '123123';
        $amount     = 100.00;
        $acctType   = 'ECHK';
        $holder     = 'Eddie Money';
        $token      = $this->createStub(ACHToken::class);

        $token->expects(self::once())
              ->method('getValue')
              ->willReturn($tokenValue);

        $token->expects(self::once())
              ->method('getAcctType')
              ->willReturn($acctType);

        $token->expects(self::once())
              ->method('getHolder')
              ->willReturn($holder);

        $client         = $this->createStub(CardConnect::class);
        $expectedParams = [
            'merchid'  => $merchId,
            'account'  => $tokenValue,
            'accttype' => $acctType,
            'name'     => $holder,
            'amount'   => $amount,
            'capture'  => 'Y',
            'address'  => '',
            'address2' => '',
            'city'     => '',
            'postal'   => '',
            'region'   => '',
            'country'  => ''
        ];

        $client->expects(self::once())
               ->method('authorizeACHTransaction')
               ->with($expectedParams)
               ->willReturn([]);

        $client->expects(self::once())
               ->method('getACHMerchantId')
               ->willReturn($merchId);

        $adapter->setClient($client);

        $adapter->createPaymentFromACHToken($token, new PaymentData($amount));
    }

    public function testRetrievePayment(): void
    {
        $adapter = new PaymentAdapter();
        $id      = 10;

        $client = $this->createStub(CardConnect::class);

        $client->expects(self::once())
               ->method('inquireTransaction')
               ->with($id)
               ->willReturn([]);

        $adapter->setClient($client);

        $payment = new MockPayment(null, $id);

        $adapter->retrievePayment($payment);
    }

    public function testRetrievePaymentReturnsNullIfTransactionIdNotSet(): void
    {
        $adapter = new PaymentAdapter();
        $payment = new MockPayment();

        $this->assertNull($adapter->retrievePayment($payment));
    }

    public function testVoidPayment(): void
    {
        $adapter = new PaymentAdapter();
        $transId = 'ABC123';
        $payment = new MockPayment(null, $transId);
        $merchId = 'A100';
        $client  = $this->createStub(CardConnect::class);

        $expectedParams = [
            'merchid' => $merchId,
            'amount'  => '0',
            'retref'  => $transId
        ];

        $client->expects(self::once())
               ->method('voidTransaction')
               ->with($expectedParams)
               ->willReturn([]);

        $client->expects(self::once())
               ->method('getMerchantId')
               ->willReturn($merchId);

        $adapter->setClient($client);

        $adapter->voidPayment($payment);
    }

    public function testRefundPayment(): void
    {
        $adapter = new PaymentAdapter();
        $amount  = 100;
        $merchId = 'ABC123';
        $transId = '123123';
        $payment = new MockPayment(null, $transId);
        $client  = $this->createStub(CardConnect::class);

        $expectedParams = [
            'merchid' => $merchId,
            'amount'  => $amount,
            'retref'  => $transId
        ];

        $client->expects(self::once())
               ->method('refundTransaction')
               ->with($expectedParams)
               ->willReturn([]);

        $client->expects(self::once())
               ->method('getMerchantId')
               ->willReturn($merchId);

        $adapter->setClient($client);

        $adapter->refundPayment($payment, $amount);
    }

    public function testRefundPaymentVoidsPayment(): void
    {
        $adapter = new PaymentAdapter();
        $amount  = 100;
        $merchId = 'ABC123';
        $transId = '123123';
        $payment = new MockPayment(null, $transId);
        $client  = $this->createStub(CardConnect::class);

        $expectedParams = [
            'merchid' => $merchId,
            'amount'  => $amount,
            'retref'  => $transId
        ];

        $client->expects(self::never())
               ->method('refundTransaction')
               ->with($expectedParams)
               ->willReturn([]);

        $client->expects(self::once())
               ->method('getMerchantId')
               ->willReturn($merchId);

        $client->expects(self::once())
               ->method('inquireTransaction')
               ->with($transId)
               ->willReturn(
                   [
                       'setlstat' => PaymentAdapter::SETTLEMENT_STATUS_QUEUED,
                       'retref'   => $transId,
                       'respcode' => '0'
                   ]
               );

        $client->expects(self::once())
               ->method('voidTransaction')
               ->willReturn([]);

        $adapter->setClient($client);

        $adapter->refundPayment($payment, $amount, true);
    }
}
