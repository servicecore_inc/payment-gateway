<?php

namespace ServiceCore\PaymentGateway\Core\Helper;

use ServiceCore\PaymentGateway\Core\Adapter\AbstractAccount;

interface AccountAdapterAwareInterface
{
    public function getAccountAdapter(): AbstractAccount;

    public function setAccountAdapter(AbstractAccount $accountAdapter);
}
