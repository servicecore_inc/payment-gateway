<?php

namespace ServiceCore\PaymentGateway\Test\Support\Stripe;

use BadMethodCallException;
use Exception;
use Stripe\Stripe;
use Stripe\Token;

/**
 * This is purely a helper class for generated card tokens for testing purposes - should not, in any way, be used in
 * production!!!
 *
 * Usage: It's a light class, you can change the fields (number, expMonth, etc) below on the fly, they're called when
 *          you request a token with getCard(). Be careful changing them, as there's technically no validation (hence
 *          the big "do not use in production" warning!). Use the card number constants below. These card numbers
 *          are taken from Stripe's documentation: @link{https://stripe.com/docs/testing#cards}
 *
 * use \Payment\Core\Test\Card as TestCard;
 *
 * TestCard::setApiKey($key);
 * TestCard::$number = TestCard::VALID_VISA;
 * $token = TestCard::getToken();
 */
class Card
{
    // These cards are all valid and should generate tokens correctly
    public const VALID_VISA               = '4242424242424242';
    public const VALID_VISA_2             = '4012888888881881';
    public const VALID_VISA_DEBIT         = '4000056655665556';
    public const VALID_MASTERCARD         = '5555555555554444';
    public const VALID_MASTERCARD_DEBIT   = '5200828282828210';
    public const VALID_MASTERCARD_PREPAID = '5105105105105100';
    public const VALID_AMERICAN_EXPRESS   = '378282246310005';
    public const VALID_AMERICAN_EXPRESS_2 = '371449635398431';
    public const VALID_DISCOVER           = '6011111111111117';
    public const VALID_DISCOVER_2         = '6011000990139424';
    public const VALID_DINERS_CLUB        = '30569309025904';
    public const VALID_DINERS_CLUB_2      = '38520000023237';
    public const VALID_JCB                = '3530111333300000';
    public const VALID_JCB_2              = '3566002020360505';

    // Charge will succeed and funds will be added directly to the available balance
    public const VALID_BYPASS_PENDING_BALANCE = '4000000000000077';

    // Charge succeeds and it will use domestic pricing - don't think this would affect us, as we're a US customer
    public const VALID_DOMESTIC_PRICING = '4000000000000093';

    // The charge succeeds, but address_line1_check and address_zip_check fails - this depends on account settings!
    public const VALID_BUT_ADDRESS_CHECK_FAILS = '4000000000000010';

    // Charge will fail with a "card_declined" error, but no specifics
    public const INVALID_CARD_DECLINED = '4000000000000002';

    // Charge will fail and provide a fraudulent reason
    public const INVALID_CARD_DECLINED_FRAUDULENT = '4100000000000019';

    // Charge fails with an 'incorrect_cvc' code
    public const INVALID_CVC_INCORRECT = '4000000000000127';

    // Charge fails with an 'expired_card' code
    public const INVALID_EXPIRED = '4000000000000069';

    // Charge fails with a 'processing_error' code
    public const INVALID_PROCESSING_ERROR = '4000000000000119';

    public static $number   = '4242424242424242';
    public static $expYear  = '2025';
    public static $expMonth = '1';
    public static $cvc      = '123';

    public static function getToken($number = null): Token
    {
        if (\defined('APP_ENV') && \getenv('APP_ENV') === 'production') {
            throw new Exception(\sprintf('%s cannot be used in production!', __FUNCTION__));
        }

        if (!Stripe::getApiKey()) {
            throw new BadMethodCallException('Call setApiKey first');
        }

        if ($number) {
            self::$number = $number;
        }

        return Token::create([
            'card' => [
                'number'    => self::$number,
                'exp_month' => self::$expMonth,
                'exp_year'  => self::$expYear,
                'cvc'       => self::$cvc
            ]
        ])->offsetGet('id');
    }

    public static function setApiKey($key): void
    {
        Stripe::setApiKey($key);
    }
}
