<?php

namespace ServiceCore\PaymentGateway\Authorize\Gateway;

use Omnipay\AuthorizeNet\CIMGateway as AuthorizeCIMGateway;
use Omnipay\AuthorizeNet\Message\CIMGetProfileRequest;
use Omnipay\Common\Message\RequestInterface;
use ServiceCore\PaymentGateway\Authorize\Message\CIMCreateCardRequest;
use ServiceCore\PaymentGateway\Authorize\Message\CIMCreatePaymentProfileRequest;
use ServiceCore\PaymentGateway\Authorize\Message\CIMGetPaymentProfileRequest;

class CIMGateway extends AuthorizeCIMGateway
{
    public function getCustomerProfile(array $params = []): RequestInterface
    {
        return $this->createRequest(CIMGetProfileRequest::class, $params);
    }

    public function getPaymentProfile(array $parameters = []): RequestInterface
    {
        return $this->createRequest(CIMGetPaymentProfileRequest::class, $parameters);
    }

    public function createCard(array $parameters = []): RequestInterface
    {
        return $this->createRequest(CIMCreateCardRequest::class, $parameters);
    }

    public function createCardFromProfile(array $parameters = []): RequestInterface
    {
        return $this->createRequest(CIMCreatePaymentProfileRequest::class, $parameters);
    }

    public function completeAuthorize(array $options = []): RequestInterface
    {
        return parent::completeAuthorize($options);
    }

    public function completePurchase(array $options = []): RequestInterface
    {
        return parent::completePurchase($options);
    }

    public function updateCard(array $options = []): RequestInterface
    {
        return parent::updateCard($options);
    }
}
