<?php

namespace ServiceCore\PaymentGateway\Stripe\Adapter;

use ServiceCore\PaymentGateway\Core\Adapter\AbstractAccount;
use ServiceCore\PaymentGateway\Core\Data\CreditCardUser;
use Stripe\Account as StripeAccount;
use Stripe\Customer as StripeCustomer;
use Stripe\Event as StripeEvent;

class Account extends AbstractAccount
{
    public function createAccount(array $params, array $options = []): StripeAccount
    {
        return StripeAccount::create($params);
    }

    public function retrieveAccount(string $id): StripeAccount
    {
        return StripeAccount::retrieve($id);
    }

    public function deleteAccount(string $id): StripeAccount
    {
        return StripeAccount::retrieve($id)->delete();
    }

    public function updateAccount(string $id, array $params): StripeAccount
    {
        return StripeAccount::update($id, $params);
    }

    public function createCustomer(CreditCardUser $ccEntity, string $accountId): StripeCustomer
    {
        return StripeCustomer::create(
            [
                'description' => \sprintf('%s %s', $ccEntity->getFirstName(), $ccEntity->getLastName()),
            ],
            [
                'stripe_account' => $accountId
            ]
        );
    }

    public function retrieveCustomer(CreditCardUser $ccEntity, string $accountId): StripeCustomer
    {
        return StripeCustomer::retrieve(
            $ccEntity->getGatewayId(),
            [
                'stripe_account' => $accountId
            ]
        );
    }

    public function retrieveEvent(string $id): StripeEvent
    {
        return StripeEvent::retrieve($id);
    }
}
