<?php

namespace ServiceCore\PaymentGateway\Test\Support;

use ServiceCore\PaymentGateway\Core\Data\CreditCard;
use ServiceCore\PaymentGateway\Core\Data\CreditCardUser;

class MockCreditCard implements CreditCard
{
    /** @var null|CreditCardUser */
    private $cardUser;

    /** @var null|string */
    private $token;

    /** @var null|string */
    private $expMonth;

    /** @var null|string */
    private $expYear;

    public function __construct(
        ?CreditCardUser $cardUser = null,
        ?string $token = null,
        ?string $expMonth = null,
        ?string $expYear = null
    ) {
        $this->cardUser = $cardUser;
        $this->token    = $token;
        $this->expMonth = $expMonth;
        $this->expYear  = $expYear;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getExpMonth(): ?string
    {
        return $this->expMonth;
    }

    public function getExpYear(): ?string
    {
        return $this->expYear;
    }

    public function getCreditCardUser(): CreditCardUser
    {
        return $this->cardUser ?: new MockCreditCardUser();
    }
}
