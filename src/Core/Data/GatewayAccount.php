<?php

namespace ServiceCore\PaymentGateway\Core\Data;

interface GatewayAccount
{
    public function getAccountId();

    public function setAccountId(string $accountId);

    public function getPublishableKey(): ?string;

    public function setPublishableKey(?string $publishableKey = null);

    public function getSecretKey(): ?string;

    public function setSecretKey(?string $secretKey = null);
}
