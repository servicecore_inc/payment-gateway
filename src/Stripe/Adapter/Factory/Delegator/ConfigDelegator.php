<?php

namespace ServiceCore\PaymentGateway\Stripe\Adapter\Factory\Delegator;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use ServiceCore\PaymentGateway\Stripe\Adapter\Config as ConfigAdapter;

class ConfigDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        // grab this class from the container to call Stripe::setApiKey()
        $container->get(ConfigAdapter::class);

        return $callback();
    }
}
