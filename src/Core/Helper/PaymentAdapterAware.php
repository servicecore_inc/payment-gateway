<?php

namespace ServiceCore\PaymentGateway\Core\Helper;

use RuntimeException;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractPayment;

trait PaymentAdapterAware
{
    /** @var AbstractPayment */
    private $paymentAdapter;

    public function getPaymentAdapter(): AbstractPayment
    {
        if (!$this->paymentAdapter) {
            throw new RuntimeException('Payment adapter is not set');
        }

        return $this->paymentAdapter;
    }

    public function setPaymentAdapter(AbstractPayment $paymentAdapter): self
    {
        $this->paymentAdapter = $paymentAdapter;

        return $this;
    }
}
