<?php

namespace ServiceCore\PaymentGateway\Core\EventHandler;

use ServiceCore\PaymentGateway\Core\Event\EventInterface;

interface EventHandlerInterface
{
    public function __invoke(EventInterface $event);
}
