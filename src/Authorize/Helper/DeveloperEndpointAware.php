<?php

namespace ServiceCore\PaymentGateway\Authorize\Helper;

trait DeveloperEndpointAware
{
    /** @var bool */
    private $useDeveloperEndpoint = false;

    public function getUseDeveloperEndpoint(): bool
    {
        return $this->useDeveloperEndpoint;
    }

    public function setUseDeveloperEndpoint(bool $useDeveloperEndpoint): self
    {
        $this->useDeveloperEndpoint = $useDeveloperEndpoint;

        return $this;
    }
}
