<?php

namespace ServiceCore\PaymentGateway\Core\Helper;

use ServiceCore\PaymentGateway\Core\Adapter\AbstractProfile;

interface ProfileAdapterAwareInterface
{
    public function getProfileAdapter(): AbstractProfile;

    public function setProfileAdapter(AbstractProfile $paymentAdapter);
}
