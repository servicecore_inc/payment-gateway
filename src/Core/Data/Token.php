<?php

namespace ServiceCore\PaymentGateway\Core\Data;

class Token
{
    /**
     * The value of the token
     *
     * @var string
     */
    private $value;

    /**
     * The expiration of the token
     *
     * @var null|string
     */
    private $expiry;

    /**
     * The owner of the token
     *
     * @var null|string
     */
    private $authority;

    public function __construct(string $value, ?string $expiry = null, ?string $authority = null)
    {
        $this->value     = $value;
        $this->expiry    = $expiry;
        $this->authority = $authority;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function getExpiry(): ?string
    {
        return $this->expiry;
    }

    public function getAuthority(): ?string
    {
        return $this->authority;
    }
}
