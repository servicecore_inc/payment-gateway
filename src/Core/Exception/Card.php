<?php

namespace ServiceCore\PaymentGateway\Core\Exception;

use Exception;

class Card extends Exception
{
    public static function cardEntityNotFound(string $id): Card
    {
        return new self(\sprintf('Requested card entity (id %s) was not found', $id));
    }

    public static function tokenDoesNotMatchPattern(string $token, string $tokenPattern): Card
    {
        return new self(
            \sprintf(
                'Provided token (%s) does not match the pattern %s',
                $token,
                $tokenPattern
            )
        );
    }
}
