<?php

namespace ServiceCore\PaymentGateway\CardConnect\Adapter;

use RuntimeException;
use ServiceCore\PaymentGateway\CardConnect\Helper\ClientAware;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractAccount;

class Account extends AbstractAccount implements ClientAwareInterface
{
    use ClientAware;

    public function retrieveAccount(string $id): void
    {
        // We do not support retrieving authorize.net accounts
    }

    public function createAccount(array $params, array $options = []): bool
    {
        $this->setCredentials($params);

        return $this->client->validate();
    }

    public function deleteAccount(string $id): void
    {
        // We do not support deleting authorize.net accounts
    }

    public function updateAccount(string $id, array $params): bool
    {
        $this->setCredentials($params);

        return $this->client->validate();
    }

    private function setCredentials(array $params)
    {
        if (!\array_key_exists('accountId', $params)) {
            throw new RuntimeException('Missing `accountId` in params');
        }

        if (!\array_key_exists('publishableKey', $params)) {
            throw new RuntimeException('Missing `publishableKey` in params');
        }

        if (!\array_key_exists('secretKey', $params)) {
            throw new RuntimeException('Missing `secretKey` in params');
        }

        $this->client->setMerchantId($params['accountId'])
                     ->setUsername($params['publishableKey'])
                     ->setPassword($params['secretKey']);
    }
}
