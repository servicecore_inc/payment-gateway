<?php

namespace ServiceCore\PaymentGateway\Core\Adapter;

use ServiceCore\PaymentGateway\Core\Data\BankAccount;
use ServiceCore\PaymentGateway\Core\Data\GatewayAccount;
use ServiceCore\PaymentGateway\Core\Data\Token;

abstract class AbstractBank
{
    abstract public function createBank(GatewayAccount $account, Token $token);

    abstract public function deleteBank(BankAccount $bank);
}
