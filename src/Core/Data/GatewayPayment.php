<?php

namespace ServiceCore\PaymentGateway\Core\Data;

use ArrayIterator;
use ArrayObject;

class GatewayPayment extends ArrayObject
{
    public const STATUS_SETTLED         = 'settled';
    public const STATUS_PENDING         = 'pending';
    public const STATUS_PENDING_CAPTURE = 'pending capture';
    public const STATUS_DECLINED        = 'declined';
    public const STATUS_VOIDED          = 'voided';
    public const STATUS_UNKNOWN         = 'unknown';

    public function __construct(
        string $transactionId,
        string $transactionStatus,
        string $responseCode,
        ?bool $isVoidable = null,
        ?bool $isRefundable = null
    ) {
        parent::__construct(
            [
                'transactionId'     => $transactionId,
                'transactionStatus' => $transactionStatus,
                'responseCode'      => $responseCode,
                'isVoidable'        => $isVoidable,
                'isRefundable'      => $isRefundable,
            ],
            0,
            ArrayIterator::class
        );
    }

    public function getTransactionStatus(): ?string
    {
        if (!$this->offsetExists('transactionStatus')) {
            return null;
        }

        return $this->offsetGet('transactionStatus');
    }
}
