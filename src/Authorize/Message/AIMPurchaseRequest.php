<?php

namespace ServiceCore\PaymentGateway\Authorize\Message;

use Omnipay\AuthorizeNet\Message\AIMPurchaseRequest as PurchaseRequest;
use ServiceCore\PaymentGateway\Authorize\Helper\BillingAware;
use SimpleXMLElement;

class AIMPurchaseRequest extends PurchaseRequest
{
    use BillingAware;

    protected function addBillingData(SimpleXMLElement $data): SimpleXMLElement
    {
        $data = parent::addBillingData($data);

        /** @var mixed $req */
        $req = $data->transactionRequest;

        $this->setBillingInfo($req->addChild('billTo'));

        return $data;
    }
}
