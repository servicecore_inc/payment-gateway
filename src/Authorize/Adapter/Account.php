<?php

namespace ServiceCore\PaymentGateway\Authorize\Adapter;

use GuzzleHttp\Client;
use ServiceCore\PaymentGateway\Authorize\Gateway\AIMGateway;
use ServiceCore\PaymentGateway\Authorize\Helper\DeveloperEndpointAware;
use ServiceCore\PaymentGateway\Authorize\Helper\GatewayCredentialAware;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractAccount;

class Account extends AbstractAccount
{
    use DeveloperEndpointAware;
    use GatewayCredentialAware;

    /** @var Client */
    private $httpClient;

    /** @var AIMGateway */
    private $AIMGateway;

    public function __construct(Client $httpClient, AIMGateway $AIMGateway)
    {
        $this->httpClient = $httpClient;
        $this->AIMGateway = $AIMGateway;
    }

    public function createAccount(array $params, array $options = []): bool
    {
        return $this->sendTestRequest();
    }

    public function updateAccount(string $id, array $params): bool
    {
        return $this->sendTestRequest();
    }

    public function deleteAccount(string $id): void
    {
        // We do not support deleting authorize.net accounts
    }

    public function retrieveAccount(string $id): void
    {
        // We do not support retrieving authorize.net accounts
    }

    public function getHttpClient(): Client
    {
        return $this->httpClient;
    }

    public function getAIMGateway(): AIMGateway
    {
        return $this->AIMGateway;
    }

    private function sendTestRequest(): bool
    {
        if ($this->useDeveloperEndpoint) {
            $endpoint = $this->AIMGateway->getDeveloperEndpoint();
        } else {
            $endpoint = $this->AIMGateway->getLiveEndpoint();
        }

        $body = \json_encode(
            [
                'authenticateTestRequest' => [
                    'merchantAuthentication' => [
                        'name'           => $this->getAccountKey(),
                        'transactionKey' => $this->getAccountSecret()
                    ]
                ]
            ]
        );

        $response = $this->getHttpClient()->post(
            $endpoint,
            [
                'body'    => $body,
                'headers' => [
                    'Content-Type'   => 'application/json',
                    'Content-Length' => \strlen($body)
                ]
            ]
        );

        $body = $response->getBody()->getContents();

        if (0 === \strpos(\bin2hex($body), 'efbbbf')) {
            $body = \substr($body, 3);
        }

        $data = \json_decode($body, true);

        if (null === $data) {
            return false;
        }

        if (\in_array($data['messages']['resultCode'], ['Error', 3, 4], true)) {
            return false;
        }

        return true;
    }
}
