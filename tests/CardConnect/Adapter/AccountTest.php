<?php

namespace ServiceCore\PaymentGateway\Test\CardConnect\Adapter;

use PHPUnit\Framework\TestCase;
use RuntimeException;
use ServiceCore\PaymentGateway\CardConnect\Adapter\Account as AccountAdapter;
use ServiceCore\PaymentGateway\CardConnect\Service\CardConnect;

/**
 * @group card-connect
 * @group account
 * @group account-card-connect
 */
class AccountTest extends TestCase
{
    public function testCreateAccountThrowsExceptionIfMissingAccountId(): void
    {
        $adapter = new AccountAdapter();
        $params  = [
            'publishableKey' => 'abc123',
            'secretKey'      => '123abc'
        ];

        $builder = $this->getMockBuilder(CardConnect::class);

        $builder->disableOriginalConstructor();

        $client = $builder->getMock();

        $adapter->setClient($client);

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Missing `accountId` in params');

        $adapter->createAccount($params);
    }

    public function testCreateAccountThrowsExceptionIfMissingPublishableKey(): void
    {
        $adapter = new AccountAdapter();
        $params  = [
            'accountId' => 'abc',
            'secretKey' => '123abc'
        ];

        $builder = $this->getMockBuilder(CardConnect::class);

        $builder->disableOriginalConstructor();

        $client = $builder->getMock();

        $adapter->setClient($client);

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Missing `publishableKey` in params');

        $adapter->createAccount($params);
    }

    public function testCreateAccountThrowsExceptionIfMissingSecretKey(): void
    {
        $adapter = new AccountAdapter();
        $params  = [
            'accountId'      => 'abc',
            'publishableKey' => 'abc123',
        ];

        $builder = $this->getMockBuilder(CardConnect::class);

        $builder->disableOriginalConstructor();

        $client = $builder->getMock();

        $adapter->setClient($client);

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Missing `secretKey` in params');

        $adapter->createAccount($params);
    }

    public function testCreateAccount(): void
    {
        $accountId      = 'abc';
        $publishableKey = 'abc123';
        $secretKey      = '123abc';

        $adapter = new AccountAdapter();
        $params  = [
            'accountId'      => $accountId,
            'publishableKey' => $publishableKey,
            'secretKey'      => $secretKey
        ];

        $builder = $this->getMockBuilder(CardConnect::class);

        $builder->disableOriginalConstructor();

        $builder->onlyMethods(
            [
                'setMerchantId',
                'setUsername',
                'setPassword',
                'validate'
            ]
        );

        $client = $builder->getMock();

        $client->expects($this->once())
               ->method('setMerchantId')
               ->with($accountId)
               ->willReturnSelf();

        $client->expects($this->once())
               ->method('setUsername')
               ->with($publishableKey)
               ->willReturnSelf();

        $client->expects($this->once())
               ->method('setPassword')
               ->with($secretKey)
               ->willReturnSelf();

        $client->expects($this->once())
               ->method('validate')
               ->willReturn(true);

        $adapter->setClient($client);

        $this->assertTrue($adapter->createAccount($params));
    }

    public function testUpdateAccount(): void
    {
        $accountId      = 'abc';
        $publishableKey = 'abc123';
        $secretKey      = '123abc';

        $adapter = new AccountAdapter();
        $params  = [
            'accountId'      => $accountId,
            'publishableKey' => $publishableKey,
            'secretKey'      => $secretKey
        ];

        $builder = $this->getMockBuilder(CardConnect::class);

        $builder->disableOriginalConstructor();

        $builder->onlyMethods(
            [
                'setMerchantId',
                'setUsername',
                'setPassword',
                'validate'
            ]
        );

        $client = $builder->getMock();

        $client->expects($this->once())
               ->method('setMerchantId')
               ->with($accountId)
               ->willReturnSelf();

        $client->expects($this->once())
               ->method('setUsername')
               ->with($publishableKey)
               ->willReturnSelf();

        $client->expects($this->once())
               ->method('setPassword')
               ->with($secretKey)
               ->willReturnSelf();

        $client->expects($this->once())
               ->method('validate')
               ->willReturn(true);

        $adapter->setClient($client);

        $this->assertTrue($adapter->updateAccount(1, $params));
    }
}
