<?php

namespace ServiceCore\PaymentGateway\Test\CardConnect\Service;

use Laminas\Http\Client as HttpClient;
use Laminas\Http\Request as HttpRequest;
use Laminas\Http\Response as HttpResponse;
use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\CardConnect\Service\CardConnect;

class CardConnectTest extends TestCase
{
    public function testProfileGet(): void
    {
        $client     = $this->createStub(HttpClient::class);
        $response   = $this->createStub(HttpResponse::class);
        $url        = 'https://foo.bar';
        $service    = new CardConnect($client, $url);
        $profileId  = 'ABC123';
        $merchantId = 'fooBar';
        $content    = \json_encode(['foo' => 'bar'], \JSON_THROW_ON_ERROR);

        $client->expects($this->once())
               ->method('setOptions');

        $client->expects($this->once())
               ->method('dispatch')
               ->with(
                   $this->callback(function (HttpRequest $zendRequest) use ($profileId, $merchantId) {
                       $this->assertEquals("/profile/{$profileId}//{$merchantId}", $zendRequest->getUri()->getPath());
                       $this->assertEquals('GET', $zendRequest->getMethod());

                       return true;
                   })
               )
               ->willReturn($response);

        $response->expects($this->once())
                 ->method('getStatusCode')
                 ->willReturn(200);

        $response->expects($this->once())
                 ->method('getBody')
                 ->willReturn($content);

        $service->setMerchantId($merchantId)->setUsername('foo')->setPassword('bar');

        $response = $service->profileGet($profileId);

        $this->assertArrayHasKey('foo', $response);
        $this->assertEquals('bar', $response['foo']);
    }

    public function testAccountGet(): void
    {
        $client     = $this->createStub(HttpClient::class);
        $response   = $this->createStub(HttpResponse::class);
        $url        = 'https://foo.bar';
        $service    = new CardConnect($client, $url);
        $profileId  = 'ABC123';
        $accountId  = '123ABC';
        $merchantId = 'fooBar';
        $content    = \json_encode(['foo' => 'bar'], \JSON_THROW_ON_ERROR);

        $client->expects($this->once())
               ->method('setOptions');

        $client->expects($this->once())
               ->method('dispatch')
               ->with(
                   $this->callback(function (HttpRequest $zendRequest) use ($profileId, $accountId, $merchantId) {
                       $this->assertEquals(
                           "/profile/{$profileId}/{$accountId}/{$merchantId}",
                           $zendRequest->getUri()->getPath()
                       );
                       $this->assertEquals('GET', $zendRequest->getMethod());

                       return true;
                   })
               )
               ->willReturn($response);

        $response->expects($this->once())
                 ->method('getStatusCode')
                 ->willReturn(200);

        $response->expects($this->once())
                 ->method('getBody')
                 ->willReturn($content);

        $service->setMerchantId($merchantId)->setUsername('foo')->setPassword('bar');

        $service->accountGet($profileId, $accountId);
    }
}
