<?php

namespace ServiceCore\PaymentGateway\CardConnect\Service;

use JsonException;
use Laminas\Http\Client;
use Laminas\Http\Header\Accept;
use Laminas\Http\Header\Authorization;
use Laminas\Http\Header\ContentType;
use Laminas\Http\Request as ZendRequest;
use Laminas\Http\Response;
use RuntimeException;

class CardConnect
{
    public const RESPONSE_ERROR = 'Malformed response. Please try again and contact support if the issue persists.';

    private const ENDPOINT_VALIDATE   = 'inquire';
    private const ENDPOINT_AUTH       = 'auth';
    private const ENDPOINT_CAPTURE    = 'capture';
    private const ENDPOINT_VOID       = 'void';
    private const ENDPOINT_REFUND     = 'refund';
    private const ENDPOINT_INQUIRE    = 'inquire';
    private const ENDPOINT_SETTLESTAT = 'settlestat';
    private const ENDPOINT_PROFILE    = 'profile';

    /** @var Client */
    private $client;

    /** @var string */
    private $url;

    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /** @var string */
    private $merchantId;

    private ?string $achUsername = null;
    private ?string $achPassword = null;
    private ?string $achMerchantId = null;

    public function __construct(Client $client, string $url)
    {
        $this->client = $client;
        $this->url    = $url;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getMerchantId(): string
    {
        return $this->merchantId;
    }

    public function setMerchantId(string $merchantId): self
    {
        $this->merchantId = $merchantId;

        return $this;
    }

    public function setACHUsername(?string $username = null): self
    {
        $this->achUsername = $username;

        return $this;
    }

    public function setACHPassword(?string $password = null): self
    {
        $this->achPassword = $password;

        return $this;
    }

    public function getACHMerchantId(): ?string
    {
        return $this->achMerchantId;
    }

    public function setACHMerchantId(?string $merchantId = null): self
    {
        $this->achMerchantId = $merchantId;

        return $this;
    }

    /**
     * Sends a request to validate the supplied credentials
     *
     * @return bool Bool representing success or failure
     */
    public function validate(): bool
    {
        try {
            $this->send(self::ENDPOINT_VALIDATE . '/0/' . $this->merchantId, ZendRequest::METHOD_GET, []);

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Sends an Authorize Transaction request via REST
     *
     * @param array $request Array representing an authorization request
     *
     * @return array Array representing an authorization response
     */
    public function authorizeTransaction(array $request): array
    {
        return $this->send(self::ENDPOINT_AUTH, ZendRequest::METHOD_PUT, $request);
    }

    /**
     * Sends an ACH Authorize Transaction request via REST
     *
     * @param array $request Array representing an authorization request
     *
     * @return array Array representing an authorization response
     */
    public function authorizeACHTransaction(array $request): array
    {
        return $this->send(self::ENDPOINT_AUTH, ZendRequest::METHOD_PUT, $request, true);
    }

    /**
     * Sends a Capture Transaction request via REST
     *
     * @param array $request Array representing a capture request
     *
     * @return array Array representing a capture response
     */
    public function captureTransaction(array $request): array
    {
        return $this->send(self::ENDPOINT_CAPTURE, ZendRequest::METHOD_PUT, $request);
    }

    /**
     * Sends a Void Transaction request via REST
     *
     * @param array $request Array representing a void request
     *
     * @return array Array representing a void response
     */
    public function voidTransaction(array $request): array
    {
        return $this->send(self::ENDPOINT_VOID, ZendRequest::METHOD_PUT, $request);
    }

    /**
     * Sends a Refund Transaction request via REST
     *
     * @param array $request Array representing a refund request
     *
     * @return array Array representing a refund response
     */
    public function refundTransaction(array $request): array
    {
        return $this->send(self::ENDPOINT_REFUND, ZendRequest::METHOD_PUT, $request);
    }

    /**
     * Sends an Inquire Transaction request via REST
     *
     * @param string $ref RetRef from previous authorization/capture response
     *
     * @return array Array representing an inquire response
     */
    public function inquireTransaction(string $ref): array
    {
        $url = self::ENDPOINT_INQUIRE . '/' . $ref . '/' . $this->merchantId;

        return $this->send($url, ZendRequest::METHOD_GET, []);
    }

    /**
     * Sends a Settlement Status request via REST
     *
     * @param string $date Settlement Date
     *
     * @return array Array representing the requested settlement status
     */
    public function settlementStatus(string $date): array
    {
        $url = self::ENDPOINT_SETTLESTAT . '?date=' . $date . '&merchid=' . $this->merchantId;

        return $this->send($url, ZendRequest::METHOD_GET, []);
    }

    public function profileGet(string $profileId): array
    {
        $url = self::ENDPOINT_PROFILE . '/' . $profileId . '//' . $this->merchantId;

        return $this->send($url, ZendRequest::METHOD_GET, []);
    }

    public function accountGet(string $profileId, string $accountId): array
    {
        $url = self::ENDPOINT_PROFILE . '/' . $profileId . '/' . $accountId . '/' . $this->merchantId;

        return $this->send($url, ZendRequest::METHOD_GET, []);
    }

    /**
     * Deletes the specified profile via REST
     *
     * @param string $profileId Profile ID
     * @param string $accountId Optional Account ID
     *
     * @return array Array representing the results of the profile deletion
     */
    public function profileDelete(string $profileId, string $accountId): array
    {
        $url = self::ENDPOINT_PROFILE . '/' . $profileId . '/' . $accountId . '/' . $this->merchantId;

        return $this->send($url, ZendRequest::METHOD_DELETE, []);
    }

    /**
     * Creates or updates a profile via REST
     *
     * @param array $request Array representing the Profile create/update request
     *
     * @return array Array representing the profile creation
     */
    public function profileCreate(array $request): array
    {
        return $this->send(self::ENDPOINT_PROFILE, ZendRequest::METHOD_PUT, $request);
    }

    private function send(string $endpoint, string $method, array $request, bool $isACH = false): array
    {
        if (!$this->merchantId || !$this->username || !$this->password) {
            throw new RuntimeException('Request to CardConnect API is missing merchant info. Did you set it?');
        }

        $username = $this->username;
        $password = $this->password;

        if ($isACH) {
            if (!$this->achMerchantId || !$this->achUsername || !$this->achPassword) {
                throw new RuntimeException(
                    'Request to CardConnect API is missing merchant info for ACH. Did you set it?'
                );
            }

            $username = $this->achUsername;
            $password = $this->achPassword;
        }

        $authToken = $this->getAuthToken($username, $password);

        $zendRequest = new ZendRequest();
        $zendRequest->getHeaders()->addHeader(new ContentType('application/json'));
        $zendRequest->getHeaders()->addHeader(new Authorization('Basic ' . $authToken));
        $zendRequest->getHeaders()->addHeader((new Accept())->addMediaType('application/json'));
        $zendRequest->setUri($this->url . '/' . $endpoint);
        $zendRequest->setMethod($method);

        if ($request) {
            $zendRequest->setContent(\json_encode($request));
        }

        $this->client->setOptions(
            [
                'timeout' => 30
            ]
        );

        $response = $this->client->dispatch($zendRequest);

        if ($response instanceof Response && $response->getStatusCode() !== 200) {
            throw new RuntimeException(
                "Request to CardConnect API failed with status code {$response->getStatusCode()}."
            );
        }

        try {
            $response = \json_decode($response->getBody(), true, 512, \JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $response['Error'] = true;
        }

        return $response;
    }

    private function getAuthToken(string $username, string $password): string
    {
        return \base64_encode($username . ':' . $password);
    }
}
