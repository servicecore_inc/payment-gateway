<?php

namespace ServiceCore\PaymentGateway\Core\Data;

class ACHToken extends Token
{
    private string $holder;
    private string $acctType;

    public function __construct(
        string $value,
        string $holder,
        string $acctType,
        ?string $expiry = null,
        ?string $authority = null
    ) {
        parent::__construct($value, $expiry, $authority);

        $this->holder   = $holder;
        $this->acctType = $acctType;
    }

    public function getHolder(): string
    {
        return $this->holder;
    }

    public function getAcctType(): string
    {
        return $this->acctType;
    }
}
