<?php

namespace ServiceCore\PaymentGateway\CardConnect\Adapter\Delegator;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use ServiceCore\PaymentGateway\CardConnect\Adapter\Profile as ProfileAdapter;

class ProfileDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null): object
    {
        $context = $callback();

        if (\method_exists($context, 'setAdapter')) {
            $adapter = $container->get(ProfileAdapter::class);

            $context->setAdapter($adapter);
        }

        return $context;
    }
}
