<?php

namespace ServiceCore\PaymentGateway\CardConnect\Data;

use DateTime;
use InvalidArgumentException;

class Expiry
{
    /** @var string */
    private $month;

    /** @var string */
    private $year;

    public function __construct(string $expiry)
    {
        $dateTime = null;

        // Per documentation, CardConnect expiry strings can be in either of the two formats below
        if (\strlen($expiry) === 4) {
            $dateTime = DateTime::createFromFormat('my', $expiry);
        } elseif (\strlen($expiry) === 8) {
            $dateTime = DateTime::createFromFormat('Ymd', $expiry);
        } else {
            throw new InvalidArgumentException('Expiry received from CardConnect has an unexpected format');
        }

        $this->month = $dateTime->format('n');
        $this->year  = $dateTime->format('Y');
    }

    public function getMonth(): string
    {
        return $this->month;
    }

    public function getYear(): string
    {
        return $this->year;
    }
}
