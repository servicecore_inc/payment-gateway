<?php

namespace ServiceCore\PaymentGateway\Authorize\Adapter\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\PaymentGateway\Authorize\Adapter\Profile as ProfileAdapter;
use ServiceCore\PaymentGateway\Authorize\Gateway\CIMGateway;

class Profile implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ProfileAdapter
    {
        $gateway = new CIMGateway();

        return new ProfileAdapter($gateway);
    }
}
