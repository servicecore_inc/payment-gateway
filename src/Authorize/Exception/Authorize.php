<?php

namespace ServiceCore\PaymentGateway\Authorize\Exception;

use Exception;

class Authorize extends Exception
{
    public const ENTITY_NOT_FOUND = 'E00040';
}
