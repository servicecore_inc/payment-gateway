<?php

namespace ServiceCore\PaymentGateway\Stripe\Adapter;

use Stripe\Stripe;

class Config
{
    private $requiredKeys = [
        'secret_key'      => true,
        'publishable_key' => true,
    ];

    public function __construct(array $config)
    {
        $this->validateConfig($config);

        Stripe::setApiKey($config['secret_key']);
    }

    private function validateConfig(array $config): bool
    {
        $diff = \array_diff_key($this->requiredKeys, $config);

        if (\count($diff) > 0) {
            throw new \InvalidArgumentException(
                \sprintf(
                    'The following configuration keys are required by the payment adapter, but were not present: %s',
                    \print_r($diff)
                )
            );
        }

        return true;
    }
}
