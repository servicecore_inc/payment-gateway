<?php

namespace ServiceCore\PaymentGateway\Test\Authorize\Adapter\Factory;

use Laminas\ServiceManager\ServiceManager;
use Omnipay\Common\GatewayInterface;
use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\Authorize\Adapter\Factory\Payment as PaymentAdapterFactory;
use ServiceCore\PaymentGateway\Authorize\Adapter\Payment as PaymentAdapter;

/**
 * @group factory
 * @group payment
 * @group payment-factory
 */
class PaymentTest extends TestCase
{
    public function testInvoke(): void
    {
        $factory   = new PaymentAdapterFactory();
        $container = new ServiceManager();
        $class     = $factory($container, PaymentAdapter::class);

        $this->assertInstanceOf(GatewayInterface::class, $class->getCIMGateway());
        $this->assertInstanceOf(GatewayInterface::class, $class->getAIMGateway());
    }
}
