<?php

namespace ServiceCore\PaymentGateway\Test\Support;

use ServiceCore\PaymentGateway\Core\Data\CreditCardUser;

class MockCreditCardUser implements CreditCardUser
{
    /** @var int|null */
    private $gatewayId;

    /** @var null|string */
    private $name;

    /** @var null|string */
    private $address;

    /** @var null|string */
    private $city;

    /** @var null|string */
    private $stateCode;

    /** @var null|string */
    private $zip;

    public function __construct(
        ?int $gatewayId = null,
        ?string $name = null,
        ?string $address = null,
        ?string $city = null,
        ?string $stateCode = null,
        ?string $zip = null
    ) {
        $this->gatewayId = $gatewayId;
        $this->name      = $name;
        $this->address   = $address;
        $this->city      = $city;
        $this->stateCode = $stateCode;
        $this->zip       = $zip;
    }

    public function getId(): int
    {
        return 1;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getFirstName(): string
    {
        return 'Card';
    }

    public function getLastName(): string
    {
        return 'User';
    }

    public function getGatewayId(): ?int
    {
        return $this->gatewayId;
    }

    public function hasEmails(): bool
    {
        return false;
    }

    public function getEmails(): ?array
    {
        return [];
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getStateCode(): ?string
    {
        return $this->stateCode;
    }

    public function getZip():? string
    {
        return $this->zip;
    }

    public function getGatewayAccountId(): ?string
    {
        return null;
    }
}
