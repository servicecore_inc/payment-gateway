<?php

namespace ServiceCore\PaymentGateway\Test;

use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\Module;

class ModuleTest extends TestCase
{
    public function testGetConfig(): void
    {
        $module = new Module();
        $config = $module->getConfig();

        $this->assertArrayHasKey('payment_gateway', $config);
        $this->assertArrayHasKey('service_manager', $config['payment_gateway']);
        $this->assertArrayHasKey('factories', $config['payment_gateway']['service_manager']);
        $this->assertArrayHasKey('delegators', $config['payment_gateway']['service_manager']);
    }
}
