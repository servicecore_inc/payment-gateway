<?php

namespace ServiceCore\PaymentGateway\Authorize\Message;

use Omnipay\AuthorizeNet\Message\CIMGetPaymentProfileRequest as GetPaymentProfile;

class CIMGetPaymentProfileRequest extends GetPaymentProfile
{
    public function getData()
    {
        $data = parent::getData();

        $data->unmaskExpirationDate = true;

        return $data;
    }
}
