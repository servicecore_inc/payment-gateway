<?php

namespace ServiceCore\PaymentGateway\Core\EventHandler;

use Doctrine\ORM\EntityManagerInterface;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractAccount;
use ServiceCore\PaymentGateway\Core\Event\EventInterface;

abstract class AbstractEventHandler implements EventHandlerInterface
{
    /** @var AbstractAccount */
    private $paymentGateway;

    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(AbstractAccount $paymentGateway, EntityManagerInterface $entityManager)
    {
        $this->paymentGateway = $paymentGateway;
        $this->entityManager  = $entityManager;
    }

    abstract public function __invoke(EventInterface $event): void;

    protected function getAccountAdapter(): AbstractAccount
    {
        return $this->paymentGateway;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }
}
