<?php

namespace ServiceCore\PaymentGateway\Core\Data;

interface CreditCard
{
    public function getToken();

    public function getExpMonth();

    public function getExpYear();

    public function getCreditCardUser(): CreditCardUser;
}
