<?php

namespace ServiceCore\PaymentGateway\Test\Authorize\Adapter\Factory;

use Laminas\ServiceManager\ServiceManager;
use Omnipay\Common\GatewayInterface;
use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\Authorize\Adapter\Card as CardAdapter;
use ServiceCore\PaymentGateway\Authorize\Adapter\Factory\Card as CardAdapterFactory;

/**
 * @group factory
 * @group card
 * @group card-factory
 */
class CardTest extends TestCase
{
    public function testInvoke(): void
    {
        $factory   = new CardAdapterFactory();
        $container = new ServiceManager();
        $class     = $factory($container, CardAdapter::class);

        $this->assertInstanceOf(GatewayInterface::class, $class->getCIMGateway());
    }
}
