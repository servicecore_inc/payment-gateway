<?php

namespace ServiceCore\PaymentGateway\Core\Adapter;

abstract class AbstractProfile
{
    abstract public function retrieveProfile(string $profileToken);
}
