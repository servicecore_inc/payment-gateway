<?php

namespace ServiceCore\PaymentGateway\Core\Helper;

use ServiceCore\PaymentGateway\Core\Adapter\AbstractPayment;

interface PaymentAdapterAwareInterface
{
    public function getPaymentAdapter(): AbstractPayment;

    public function setPaymentAdapter(AbstractPayment $paymentAdapter);
}
