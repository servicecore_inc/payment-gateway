<?php

namespace ServiceCore\PaymentGateway\Test\Authorize\Adapter;

use Omnipay\AuthorizeNet\Message\CIMResponse;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\Authorize\Adapter\Card as CardAdapter;
use ServiceCore\PaymentGateway\Authorize\Adapter\Payment as PaymentAdapter;
use ServiceCore\PaymentGateway\Authorize\Gateway\CIMGateway;
use ServiceCore\PaymentGateway\Authorize\Message\CIMCreateCardRequest;
use ServiceCore\PaymentGateway\Authorize\Message\CIMGetPaymentProfileRequest;
use ServiceCore\PaymentGateway\Core\Data\Token;
use ServiceCore\PaymentGateway\Test\Support\MockCreditCard;
use ServiceCore\PaymentGateway\Test\Support\MockCreditCardUser;

/**
 * @group authorize
 * @group card
 * @group card-authorize
 */
class CardTest extends TestCase
{
    public function testCreateCardReturnsCardResponse(): void
    {
        $gateway    = $this->getMockGateway();
        $adapter    = new CardAdapter($gateway);
        $cardUser   = new MockCreditCardUser();
        $tokenValue = 'abc';
        $token      = new Token($tokenValue);
        $builder    = $this->getMockBuilder(CIMCreateCardRequest::class);

        $builder->disableOriginalConstructor();
        $builder->onlyMethods(['send']);

        $request = $builder->getMock();
        $builder = $this->getMockBuilder(CIMResponse::class);

        $builder->disableOriginalConstructor();

        $response = $builder->getMock();

        $request->expects($this->once())
                ->method('send')
                ->willReturn($response);

        $gateway->expects($this->never())
                ->method('createCardFromProfile');

        $billingData = [
            'firstName' => $cardUser->getFirstName(),
            'lastName'  => $cardUser->getLastName(),
            'address'   => $cardUser->getAddress(),
            'city'      => $cardUser->getCity(),
            'state'     => $cardUser->getStateCode(),
            'zip'       => $cardUser->getZip()
        ];

        $expectedParams = [
            'email'                => null,
            'description'          => null,
            'opaqueDataValue'      => $tokenValue,
            'opaqueDataDescriptor' => PaymentAdapter::PAYMENT_DESCRIPTOR,
            'billingData'          => $billingData
        ];

        $gateway->expects($this->once())
                ->method('createCard')
                ->with($expectedParams)
                ->willReturn($request);

        $adapter->createCard($cardUser, $token);
    }

    public function testCreateCardSetsTokenAuthority(): void
    {
        $gateway    = $this->getMockGateway();
        $adapter    = new CardAdapter($gateway);
        $cardUser   = new MockCreditCardUser();
        $tokenValue = 'abc';
        $authority  = 'Bob Johnson';
        $token      = new Token($tokenValue, null, $authority);
        $builder    = $this->getMockBuilder(CIMCreateCardRequest::class);

        $builder->disableOriginalConstructor();
        $builder->onlyMethods(['send']);

        $request = $builder->getMock();
        $builder = $this->getMockBuilder(CIMResponse::class);

        $builder->disableOriginalConstructor();

        $response = $builder->getMock();

        $request->expects($this->once())
                ->method('send')
                ->willReturn($response);

        $gateway->expects($this->never())
                ->method('createCardFromProfile');

        $billingData = [
            'firstName' => 'Bob',
            'lastName'  => 'Johnson',
            'address'   => $cardUser->getAddress(),
            'city'      => $cardUser->getCity(),
            'state'     => $cardUser->getStateCode(),
            'zip'       => $cardUser->getZip()
        ];

        $expectedParams = [
            'email'                => null,
            'description'          => null,
            'opaqueDataValue'      => $tokenValue,
            'opaqueDataDescriptor' => PaymentAdapter::PAYMENT_DESCRIPTOR,
            'billingData'          => $billingData
        ];

        $gateway->expects($this->once())
                ->method('createCard')
                ->with($expectedParams)
                ->willReturn($request);

        $adapter->createCard($cardUser, $token);
    }

    public function testCreateCardReturnsProfileResponse(): void
    {
        $gateway    = $this->getMockGateway();
        $adapter    = new CardAdapter($gateway);
        $cardUser   = new MockCreditCardUser(1);
        $tokenValue = 'abc';
        $data       = new Token($tokenValue);
        $builder    = $this->getMockBuilder(CIMCreateCardRequest::class);

        $builder->disableOriginalConstructor();
        $builder->onlyMethods(['send']);

        $request = $builder->getMock();
        $builder = $this->getMockBuilder(CIMResponse::class);

        $builder->disableOriginalConstructor();

        $response = $builder->getMock();

        $request->expects($this->once())
                ->method('send')
                ->willReturn($response);

        $billingData = [
            'firstName' => $cardUser->getFirstName(),
            'lastName'  => $cardUser->getLastName(),
            'address'   => $cardUser->getAddress(),
            'city'      => $cardUser->getCity(),
            'state'     => $cardUser->getStateCode(),
            'zip'       => $cardUser->getZip()
        ];

        $expectedParams = [
            'email'                => null,
            'description'          => null,
            'opaqueDataValue'      => $tokenValue,
            'opaqueDataDescriptor' => PaymentAdapter::PAYMENT_DESCRIPTOR,
            'billingData'          => $billingData,
            'customerProfileId'    => 1
        ];

        $gateway->expects($this->once())
                ->method('createCardFromProfile')
                ->with($expectedParams)
                ->willReturn($request);

        $gateway->expects($this->never())
                ->method('createCard');

        $adapter->createCard($cardUser, $data);
    }

    public function testDeleteCard(): void
    {
        $gateway = $this->getMockGateway();
        $adapter = new CardAdapter($gateway);
        $card    = new MockCreditCard();
        $builder = $this->getMockBuilder(CIMCreateCardRequest::class);

        $builder->disableOriginalConstructor();
        $builder->onlyMethods(['send']);

        $request = $builder->getMock();
        $builder = $this->getMockBuilder(CIMResponse::class);

        $builder->disableOriginalConstructor();

        $response = $builder->getMock();

        $request->expects($this->once())
                ->method('send')
                ->willReturn($response);

        $gateway->expects($this->once())
                ->method('deleteCard')
                ->willReturn($request);

        $adapter->deleteCard($card);
    }

    public function testRetrieveCard(): void
    {
        $gateway              = $this->getMockGateway();
        $adapter              = new CardAdapter($gateway);
        $cardProfileToken     = '123abc';
        $customerProfileToken = 'abc123';

        $request = $this->createStub(CIMGetPaymentProfileRequest::class);

        $response = $this->createStub(CIMResponse::class);

        $request->expects($this->once())
        ->method('send')
        ->willReturn($response);

        $gateway->expects($this->once())
        ->method('getPaymentProfile')
        ->with([
            'customerProfileId'        => $customerProfileToken,
            'customerPaymentProfileId' => $cardProfileToken
        ])
        ->willReturn($request);

        $adapter->retrieveCard($customerProfileToken, $cardProfileToken);
    }

  /**
   * @return MockObject|CIMGateway
   */
    private function getMockGateway(): MockObject
    {
        $builder = $this->getMockBuilder(CIMGateway::class);

        $builder->disableOriginalConstructor();
        $builder->onlyMethods(
            [
                'setApiLoginId',
                'setTransactionKey',
                'setDeveloperMode',
                'getPaymentProfile',
                'createCardFromProfile',
                'createCard',
                'deleteCard'
            ]
        );

        $mock = $builder->getMock();

        $mock->expects($this->any())
             ->method('setApiLoginId')
             ->willReturnSelf();

        $mock->expects($this->any())
             ->method('setTransactionKey')
             ->willReturnSelf();

        $mock->expects($this->any())
             ->method('setDeveloperMode')
             ->willReturnSelf();

        return $mock;
    }
}
