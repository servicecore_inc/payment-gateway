<?php

namespace ServiceCore\PaymentGateway\CardConnect\Helper;

use RuntimeException;
use ServiceCore\PaymentGateway\CardConnect\Service\CardConnect;

trait ClientAware
{
    /** @var CardConnect|null */
    private $client;

    public function getClient(): CardConnect
    {
        if (!$this->client) {
            throw new RuntimeException('CardConnect client is not set!');
        }

        return $this->client;
    }

    public function setClient(CardConnect $client): self
    {
        $this->client = $client;

        return $this;
    }
}
