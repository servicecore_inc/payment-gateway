<?php

namespace ServiceCore\PaymentGateway\CardConnect\Adapter;

use ServiceCore\PaymentGateway\CardConnect\Service\CardConnect;

interface ClientAwareInterface
{
    public function getClient(): CardConnect;

    public function setClient(CardConnect $client);
}
