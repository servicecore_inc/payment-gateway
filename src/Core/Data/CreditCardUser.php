<?php

namespace ServiceCore\PaymentGateway\Core\Data;

interface CreditCardUser
{
    public function getId();

    public function getName();

    public function getFirstName();

    public function getLastName();

    public function getGatewayId();

    public function hasEmails(): bool;

    public function getEmails(): ?array;

    public function getAddress();

    public function getCity();

    public function getStateCode();

    public function getZip();

    public function getGatewayAccountId(): ?string;
}
