<?php

namespace ServiceCore\PaymentGateway\Stripe\EventHandler\Charge;

use ServiceCore\PaymentGateway\Core\Event\EventInterface;
use ServiceCore\PaymentGateway\Core\EventHandler\AbstractEventHandler;

class Succeeded extends AbstractEventHandler
{
    public function __invoke(EventInterface $event): void
    {
    }
}
