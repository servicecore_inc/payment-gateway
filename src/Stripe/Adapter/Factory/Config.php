<?php

namespace ServiceCore\PaymentGateway\Stripe\Adapter\Factory;

use Interop\Container\ContainerInterface;
use InvalidArgumentException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\PaymentGateway\Stripe\Adapter\Config as PaymentConfig;

class Config implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): PaymentConfig
    {
        if (!$container->has('stripe')) {
            throw new InvalidArgumentException('Configuration key "stripe" missing.');
        }

        return new PaymentConfig($container->get('stripe'));
    }
}
