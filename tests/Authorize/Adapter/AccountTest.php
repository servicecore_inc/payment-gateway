<?php

namespace ServiceCore\PaymentGateway\Test\Authorize\Adapter;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Stream;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\Authorize\Adapter\Account;
use ServiceCore\PaymentGateway\Authorize\Gateway\AIMGateway;

/**
 * @group authorize
 * @group account
 * @group account-authorize
 */
class AccountTest extends TestCase
{
    public function testCreateAccountReturnsFalseIfNullData(): void
    {
        $responseBuilder = $this->getMockBuilder(Response::class);

        $responseBuilder->disableOriginalConstructor();
        $responseBuilder->setMethods(
            [
                'getBody'
            ]
        );

        $response = $responseBuilder->getMock();
        $body     = $this->createMock(Stream::class);

        $response->expects($this->once())
                 ->method('getBody')
                 ->willReturn($body);

        $clientBuilder = $this->getMockBuilder(Client::class);

        $clientBuilder->disableOriginalConstructor();
        $clientBuilder->setMethods(
            [
                'post'
            ]
        );

        $client = $clientBuilder->getMock();

        $client->expects($this->once())
               ->method('post')
               ->willReturn($response);

        $account = new Account($client, $this->getMockAIMGateway());

        $this->assertFalse($account->createAccount([]));
    }

    public function testCreateAccountReturnsFalseIfErrorCodeSet(): void
    {
        $responseBuilder = $this->getMockBuilder(Response::class);

        $responseBuilder->disableOriginalConstructor();
        $responseBuilder->setMethods(
            [
                'getBody'
            ]
        );

        $response = $responseBuilder->getMock();


        $bodyBuilder = $this->getMockBuilder(Stream::class);

        $bodyBuilder->disableOriginalConstructor();
        $bodyBuilder->setMethods(
            [
                'getContents'
            ]
        );

        $body = $bodyBuilder->getMock();

        $body->expects($this->once())
             ->method('getContents')
             ->willReturn(
                 \json_encode(
                     [
                         'messages' => [
                             'resultCode' => 'Error'
                         ]
                     ]
                 )
             );

        $response->expects($this->once())
                 ->method('getBody')
                 ->willReturn($body);

        $clientBuilder = $this->getMockBuilder(Client::class);

        $clientBuilder->disableOriginalConstructor();
        $clientBuilder->setMethods(
            [
                'post'
            ]
        );

        $client = $clientBuilder->getMock();

        $client->expects($this->once())
               ->method('post')
               ->willReturn($response);

        $account = new Account($client, $this->getMockAIMGateway());

        $this->assertFalse($account->createAccount([]));
    }

    public function testCreateAccountReturnsTrue(): void
    {
        $responseBuilder = $this->getMockBuilder(Response::class);

        $responseBuilder->disableOriginalConstructor();
        $responseBuilder->setMethods(
            [
                'getBody'
            ]
        );

        $response = $responseBuilder->getMock();


        $bodyBuilder = $this->getMockBuilder(Stream::class);

        $bodyBuilder->disableOriginalConstructor();
        $bodyBuilder->setMethods(
            [
                'getContents'
            ]
        );

        $body = $bodyBuilder->getMock();

        $body->expects($this->once())
             ->method('getContents')
             ->willReturn(
                 \json_encode(
                     [
                         'messages' => [
                             'resultCode' => 'Success'
                         ]
                     ]
                 )
             );

        $response->expects($this->once())
                 ->method('getBody')
                 ->willReturn($body);

        $clientBuilder = $this->getMockBuilder(Client::class);

        $clientBuilder->disableOriginalConstructor();
        $clientBuilder->setMethods(
            [
                'post'
            ]
        );

        $client = $clientBuilder->getMock();

        $client->expects($this->once())
               ->method('post')
               ->willReturn($response);

        $account = new Account($client, $this->getMockAIMGateway());

        $this->assertTrue($account->createAccount([]));
    }

    /**
     * @return AIMGateway|MockObject
     */
    private function getMockAIMGateway(): AIMGateway
    {
        $builder = $this->getMockBuilder(AIMGateway::class);

        $builder->disableOriginalConstructor();

        return $builder->getMock();
    }
}
