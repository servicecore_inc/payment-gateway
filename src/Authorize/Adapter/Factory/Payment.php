<?php

namespace ServiceCore\PaymentGateway\Authorize\Adapter\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\PaymentGateway\Authorize\Adapter\Payment as PaymentAdapter;
use ServiceCore\PaymentGateway\Authorize\Gateway\AIMGateway;
use ServiceCore\PaymentGateway\Authorize\Gateway\CIMGateway;

class Payment implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): PaymentAdapter
    {
        $AIMGateway = new AIMGateway();
        $CIMGateway = new CIMGateway();

        return new PaymentAdapter($AIMGateway, $CIMGateway);
    }
}
