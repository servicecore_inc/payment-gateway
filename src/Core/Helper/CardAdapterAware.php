<?php

namespace ServiceCore\PaymentGateway\Core\Helper;

use RuntimeException;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractCard;

trait CardAdapterAware
{
    /** @var AbstractCard */
    private $cardAdapter;

    public function getCardAdapter(): AbstractCard
    {
        if (!$this->cardAdapter) {
            throw new RuntimeException('Card adapter is not set');
        }

        return $this->cardAdapter;
    }

    public function setCardAdapter(AbstractCard $cardAdapter)
    {
        $this->cardAdapter = $cardAdapter;

        return $this;
    }
}
