<?php

namespace ServiceCore\PaymentGateway\Authorize\Gateway;

use Omnipay\AuthorizeNet\AIMGateway as OmniAIMGateway;
use Omnipay\Common\Message\RequestInterface;
use ServiceCore\PaymentGateway\Authorize\Message\AIMPurchaseRequest;

class AIMGateway extends OmniAIMGateway
{
    public function purchase(array $parameters = []): RequestInterface
    {
        return $this->createRequest(AIMPurchaseRequest::class, $parameters);
    }

    public function completeAuthorize(array $options = []): RequestInterface
    {
        return parent::completeAuthorize($options);
    }

    public function completePurchase(array $options = []): RequestInterface
    {
        return parent::completePurchase($options);
    }

    public function deleteCard(array $options = []): RequestInterface
    {
        return parent::deleteCard($options);
    }

    public function createCard(array $options = []): RequestInterface
    {
        return parent::createCard($options);
    }

    public function updateCard(array $options = []): RequestInterface
    {
        return parent::updateCard($options);
    }
}
