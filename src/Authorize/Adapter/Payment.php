<?php

namespace ServiceCore\PaymentGateway\Authorize\Adapter;

use Omnipay\AuthorizeNet\Model\CardReference;
use Omnipay\AuthorizeNet\Model\TransactionReference;
use Omnipay\Common\Message\ResponseInterface;
use ServiceCore\PaymentGateway\Authorize\Gateway\AIMGateway;
use ServiceCore\PaymentGateway\Authorize\Gateway\CIMGateway;
use ServiceCore\PaymentGateway\Authorize\Helper\DeveloperEndpointAware;
use ServiceCore\PaymentGateway\Authorize\Helper\GatewayCredentialAware;
use ServiceCore\PaymentGateway\Core\Adapter\AbstractPayment;
use ServiceCore\PaymentGateway\Core\Data\CreditCard;
use ServiceCore\PaymentGateway\Core\Data\GatewayPayment;
use ServiceCore\PaymentGateway\Core\Data\Payment as SourcePayment;
use ServiceCore\PaymentGateway\Core\Data\PaymentData;
use ServiceCore\PaymentGateway\Core\Data\Token;

class Payment extends AbstractPayment
{
    use DeveloperEndpointAware;
    use GatewayCredentialAware;

    public const STATUS_SETTLED         = 'settledSuccessfully';
    public const STATUS_DECLINED        = 'declined';
    public const STATUS_VOIDED          = 'voided';
    public const STATUS_PENDING         = 'capturedPendingSettlement';
    public const STATUS_PENDING_CAPTURE = 'authorizedPendingCapture';

    public const PAYMENT_DESCRIPTOR = 'COMMON.ACCEPT.INAPP.PAYMENT';

    private AIMGateway $AIMGateway;
    private CIMGateway $CIMGateway;

    public function __construct(AIMGateway $AIMGateway, CIMGateway $CIMGateway)
    {
        $this->AIMGateway = $AIMGateway;
        $this->CIMGateway = $CIMGateway;
    }

    public function createPayment(CreditCard $card, PaymentData $data): ResponseInterface
    {
        $cardReference = new CardReference();

        $cardReference->setCustomerProfileId($card->getCreditCardUser()->getGatewayId());
        $cardReference->setPaymentProfileId($card->getToken());

        $params                  = [];
        $params['cardReference'] = $cardReference;
        $params['amount']        = $data->getAmount();
        $params['billingData']   = $data->getBillingData();

        if ($identifier = $data->getIdentifier()) {
            $params['invoiceNumber'] = \substr($identifier, 0, 20);
        }

        $gatewayClient = $this->CIMGateway;

        $gatewayClient->setApiLoginId($this->getAccountKey())
                      ->setTransactionKey($this->getAccountSecret())
                      ->setDeveloperMode($this->useDeveloperEndpoint);

        return $gatewayClient->purchase($params)->send();
    }

    public function createPaymentFromToken(Token $token, PaymentData $data): ResponseInterface
    {
        $params                         = [];
        $params['opaqueDataDescriptor'] = $data->getDescriptor();
        $params['opaqueDataValue']      = $token->getValue();
        $params['amount']               = $data->getAmount();
        $params['billingData']          = $data->getBillingData();

        if ($identifier = $data->getIdentifier()) {
            $params['invoiceNumber'] = \substr($identifier, 0, 20);
        }

        $gatewayClient = $this->AIMGateway;

        $gatewayClient->setApiLoginId($this->getAccountKey())
                      ->setTransactionKey($this->getAccountSecret())
                      ->setDeveloperMode($this->useDeveloperEndpoint);

        return $gatewayClient->purchase($params)->send();
    }

    public function retrievePayment(SourcePayment $payment): ?GatewayPayment
    {
        $params = [
            'transactionReference' => $payment->getTransactionId()
        ];

        $gatewayClient = $this->AIMGateway;

        $gatewayClient->setApiLoginId($this->getAccountKey())
                      ->setTransactionKey($this->getAccountSecret())
                      ->setDeveloperMode($this->useDeveloperEndpoint);

        return $this->parseGatewayPayment(
            $gatewayClient->queryDetail($params)->send()
        );
    }

    public function voidPayment(SourcePayment $payment): ResponseInterface
    {
        $params                         = [];
        $params['transactionReference'] = $payment->getTransactionId();

        $gatewayClient = $this->AIMGateway;

        $gatewayClient->setApiLoginId($this->getAccountKey())
                      ->setTransactionKey($this->getAccountSecret())
                      ->setDeveloperMode($this->useDeveloperEndpoint);

        return $gatewayClient->void($params)->send();
    }

    public function refundPayment(
        SourcePayment $payment,
        float $amount,
        bool $voidIfFails = false
    ): ResponseInterface {
        $transactionReference = new TransactionReference();

        $transactionReference->setTransId($payment->getTransactionId());
        $transactionReference->setCard(
            [
                'number' => $payment->getLastFour(),
                'expiry' => $payment->getExpiry()
            ]
        );

        $params                         = [];
        $params['transactionReference'] = $transactionReference;
        $params['amount']               = $amount;
        $params['voidIfRefundFails']    = $voidIfFails;

        $gatewayClient = $this->AIMGateway;

        $gatewayClient->setApiLoginId($this->getAccountKey())
                      ->setTransactionKey($this->getAccountSecret())
                      ->setDeveloperMode($this->useDeveloperEndpoint);

        return $gatewayClient->refund($params)->send();
    }

    public function getCIMGateway(): CIMGateway
    {
        return $this->CIMGateway;
    }

    public function getAIMGateway(): AIMGateway
    {
        return $this->AIMGateway;
    }

    private function parseGatewayPayment(ResponseInterface $response): ?GatewayPayment
    {
        if (!\property_exists($response, 'transaction')) {
            return null;
        }

        $transaction = $response->transaction;

        if (!$transaction) {
            return null;
        }

        $status = $transaction['transactionStatus'];

        switch ($status) {
            case self::STATUS_SETTLED:
                $status = GatewayPayment::STATUS_SETTLED;

                break;
            case self::STATUS_DECLINED:
                $status = GatewayPayment::STATUS_DECLINED;

                break;
            case self::STATUS_VOIDED:
                $status = GatewayPayment::STATUS_VOIDED;

                break;
            case self::STATUS_PENDING:
                $status = GatewayPayment::STATUS_PENDING;

                break;
            case self::STATUS_PENDING_CAPTURE:
                $status = GatewayPayment::STATUS_PENDING_CAPTURE;

                break;
            default:
                $status = GatewayPayment::STATUS_UNKNOWN;

                break;
        }

        return new GatewayPayment(
            $transaction['transId'],
            $status,
            $transaction['responseCode'],
        );
    }
}
