<?php

namespace ServiceCore\PaymentGateway\CardConnect\Service\Factory;

use Exception;
use Interop\Container\ContainerInterface;
use Laminas\Http\Client;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\PaymentGateway\CardConnect\Service\CardConnect as CardConnectService;

class CardConnect implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CardConnectService
    {
        $config = $container->get('card_connect');

        if (!\array_key_exists('url', $config)) {
            throw new Exception('CardConnect Url must be defined in config.');
        }

        return new CardConnectService(new Client(), $config['url']);
    }
}
