<?php

namespace ServiceCore\PaymentGateway\Authorize\Adapter\Factory;

use GuzzleHttp\Client;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\PaymentGateway\Authorize\Adapter\Account as AccountAdapter;
use ServiceCore\PaymentGateway\Authorize\Gateway\AIMGateway;

class Account implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): AccountAdapter
    {
        $client     = new Client();
        $AIMGateway = new AIMGateway();

        return new AccountAdapter($client, $AIMGateway);
    }
}
