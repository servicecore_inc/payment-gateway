<?php

namespace ServiceCore\PaymentGateway\Stripe\EventHandler\Account;

use ServiceCore\PaymentGateway\Core\Data\GatewayAccount;
use ServiceCore\PaymentGateway\Core\Event\EventInterface;
use ServiceCore\PaymentGateway\Core\EventHandler\AbstractEventHandler;

class Updated extends AbstractEventHandler
{
    public function __invoke(EventInterface $event): void
    {
        $id = $event->offsetGet('user_id');

        /** @var GatewayAccount $account */
        $account = $this->getEntityManager()->find(GatewayAccount::class, $id);

        if (!$account) {
            throw new \InvalidArgumentException(\sprintf(
                'A payment account with an ID of %s was requested, but could not be found',
                $id
            ));
        }

        $account->setChargesEnabled($event->offsetGet('charges_enabled'));
        $account->setTransfersEnabled($event->offsetGet('transfers_enabled'));

        $this->getEntityManager()->flush();
    }
}
