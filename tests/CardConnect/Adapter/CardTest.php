<?php

namespace ServiceCore\PaymentGateway\Test\CardConnect\Adapter;

use PHPUnit\Framework\TestCase;
use ServiceCore\PaymentGateway\CardConnect\Adapter\Card as CardAdapter;
use ServiceCore\PaymentGateway\CardConnect\Service\CardConnect;
use ServiceCore\PaymentGateway\Core\Data\Token;
use ServiceCore\PaymentGateway\Test\Support\MockCreditCard;
use ServiceCore\PaymentGateway\Test\Support\MockCreditCardUser;

/**
 * @group card-connect
 * @group card
 * @group card-card-connect
 */
class CardTest extends TestCase
{
    public function testCreateCard(): void
    {
        $adapter = new CardAdapter();
        $builder = $this->getMockBuilder(CardConnect::class);

        $builder->disableOriginalConstructor();

        $builder->onlyMethods(
            [
                'profileCreate',
                'getMerchantId'
            ]
        );

        $gatewayId  = 100;
        $tokenValue = 'abc123abc123';
        $merchId    = '123321';
        $name       = 'bob johnson';
        $address    = '1111 Test St';
        $city       = 'Testville';
        $region     = 'CO';
        $postal     = '80226';
        $ccEntity   = new MockCreditCardUser($gatewayId, $name, $address, $city, $region, $postal);

        $expectedProfileCreateParams = [
            'profile'     => $gatewayId,
            'merchid'     => $merchId,
            'defaultacct' => 'Y',
            'account'     => $tokenValue,
            'name'        => $name,
            'address'     => $address,
            'city'        => $city,
            'region'      => $region,
            'postal'      => $postal
        ];

        $client = $builder->getMock();

        $client->expects($this->once())
               ->method('getMerchantId')
               ->willReturn($merchId);

        $client->expects($this->once())
               ->method('profileCreate')
               ->with($expectedProfileCreateParams)
               ->willReturn(['yay']);

        $adapter->setClient($client);

        $token = new Token($tokenValue);

        $this->assertContains('yay', $adapter->createCard($ccEntity, $token));
    }

    public function testCreateCardUsesTokenAuthority(): void
    {
        $adapter = new CardAdapter();
        $builder = $this->getMockBuilder(CardConnect::class);

        $builder->disableOriginalConstructor();

        $builder->onlyMethods(
            [
                'profileCreate',
                'getMerchantId'
            ]
        );

        $authority  = 'Bob Johnston';
        $gatewayId  = 100;
        $tokenValue = 'abc123abc123';
        $merchId    = '123321';
        $name       = 'bob johnson';
        $address    = '1111 Test St';
        $city       = 'Testville';
        $region     = 'CO';
        $postal     = '80226';
        $ccEntity   = new MockCreditCardUser($gatewayId, $name, $address, $city, $region, $postal);

        $expectedProfileCreateParams = [
            'profile'     => $gatewayId,
            'merchid'     => $merchId,
            'defaultacct' => 'Y',
            'account'     => $tokenValue,
            'name'        => $authority,
            'address'     => $address,
            'city'        => $city,
            'region'      => $region,
            'postal'      => $postal
        ];

        $client = $builder->getMock();

        $client->expects($this->once())
               ->method('getMerchantId')
               ->willReturn($merchId);

        $client->expects($this->once())
               ->method('profileCreate')
               ->with($expectedProfileCreateParams)
               ->willReturn(['yay']);

        $adapter->setClient($client);

        $token = new Token($tokenValue, null, $authority);

        $this->assertContains('yay', $adapter->createCard($ccEntity, $token));
    }

    public function testDeleteCard(): void
    {
        $adapter = new CardAdapter();
        $builder = $this->getMockBuilder(CardConnect::class);

        $builder->disableOriginalConstructor();

        $builder->onlyMethods(
            [
                'profileDelete',
            ]
        );

        $client    = $builder->getMock();
        $gatewayId = 123;
        $token     = 'abc123';

        $client->expects($this->once())
               ->method('profileDelete')
               ->with($gatewayId, $token)
               ->willReturn(['yay']);

        $user = new MockCreditCardUser($gatewayId);
        $card = new MockCreditCard($user, $token);

        $adapter->setClient($client);

        $this->assertContains('yay', $adapter->deleteCard($card));
    }

    public function testRetrieveCard(): void
    {
        $adapter = new CardAdapter();
        $builder = $this->getMockBuilder(CardConnect::class);

        $builder->disableOriginalConstructor();

        $builder->onlyMethods(
            [
                'accountGet',
            ]
        );

        $client           = $builder->getMock();
        $userProfileToken = '123ABC';
        $cardProfileToken = '444DDD';

        $client->expects($this->once())
               ->method('accountGet')
               ->with($userProfileToken, $cardProfileToken)
               ->willReturn(['success']);

        $adapter->setClient($client);

        $this->assertContains('success', $adapter->retrieveCard($userProfileToken, $cardProfileToken));
    }

    public function testUpdateCard(): void
    {
        $adapter = new CardAdapter();
        $builder = $this->getMockBuilder(CardConnect::class);

        $builder->disableOriginalConstructor();

        $builder->onlyMethods(
            [
                'profileCreate',
                'getMerchantId',
            ]
        );

        $client = $builder->getMock();

        $profile   = 123;
        $cardToken = '1';
        $merchId   = '123321';

        $createData = [
            'profile'       => "$profile/$cardToken",
            'defaultacct'   => 'Y',
            'profileupdate' => 'Y',
            'merchid'       => $merchId,
        ];

        $client->expects($this->once())
               ->method('getMerchantId')
               ->willReturn($merchId);

        $client->expects($this->once())
               ->method('profileCreate')
               ->with($createData)
               ->willReturn(['success']);

        $user = new MockCreditCardUser($profile);
        $card = new MockCreditCard($user, $cardToken);

        $adapter->setClient($client);

        $this->assertContains('success', $adapter->updateCard($card));
    }
}
